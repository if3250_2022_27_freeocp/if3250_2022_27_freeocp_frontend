export const curationChosenDataDummy = [
  {
    title: 'Bab Pengantar',
    course_title: 'Dasar Assembly',
    created_at: new Date(2022, 3, 1),
    status: 'review'
  },
  {
    title: 'Latihan Notasi Dasar',
    course_title: 'Pengenalan Algoritma Dasar',
    created_at: new Date(2022, 3, 2),
    status: 'accept'
  },
  {
    title: 'Latihan Transpose Matriks',
    course_title: 'Aljabar dan Geometri',
    created_at: new Date(2022, 3, 3),
    status: 'decline'
  },
]