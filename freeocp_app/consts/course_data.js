export const courseDataDummy = [
  {
    thumbnail: 'https://atariwiki.org/wiki/attach/6502%20Assembly%20Code/6502%20Assembly%20Code%20in%20Sublime%20Text.jpg',
    title: 'Dasar Assembly',
    topic: 'Informatika',
    creator: 'Randy Zakya Suchrady Alter.',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
    created_at: new Date(2022, 1, 2),
    last_updated_at: new Date(2022, 1, 4),
    taken_count: 451
  },
  {
    thumbnail: 'https://www.todaysmedicaldevelopments.com/fileuploads/publications/21/issues/103648/articles/images/Photo-2-Bioelectronic_fmt.png',
    title: 'Bioelektronika',
    topic: 'Biomedis',
    creator: 'Gregorius Dimas Alter.',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
    created_at: new Date(2021, 3, 16),
    last_updated_at: new Date(2021, 3, 18),
    taken_count: 138
  },
  {
    thumbnail: 'https://garudacyber.co.id/an-component/media/upload-gambar-artikel/5_Algortima_dan_Struktur_Data.jpg',
    title: 'Algoritma Data',
    topic: 'Informatika',
    creator: 'Pedro Rhapsodya Alter.',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
    created_at: new Date(2021, 5, 11),
    last_updated_at: new Date(2021, 5, 18),
    taken_count: 156
  },
  {
    thumbnail: 'https://www.itb.ac.id/files/dokumentasi/1602641342-ilustrasi---fisika.png',
    title: 'Fundamental Fisika',
    topic: 'Fisika',
    creator: 'Aurelius Marcel Alter.',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
    created_at: new Date(2021, 1, 16),
    last_updated_at: new Date(2021, 1, 17),
    taken_count: 210
  },
  {
    thumbnail: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Arduino_ftdi_chip-1.jpg/390px-Arduino_ftdi_chip-1.jpg',
    title: 'Pengantar Analisis Rangkaian',
    topic: 'Elektro',
    creator: 'Christian Gunawan Alter.',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
    created_at: new Date(2021, 6, 9),
    last_updated_at: new Date(2021, 6, 15),
    taken_count: 314
  },
  {
    thumbnail: 'https://wallpaperaccess.com/full/5349190.jpg',
    title: 'Termodinamika',
    topic: 'Teknik Mesin',
    creator: 'Rinaldi Munir Alter.',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
    created_at: new Date(2021, 5, 18),
    last_updated_at: new Date(2021, 5, 23),
    taken_count: 126
  },
]