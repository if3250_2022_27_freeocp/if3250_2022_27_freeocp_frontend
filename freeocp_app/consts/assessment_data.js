export const assessmentData = {
  title: 'Latihan Matematika',
  questions: [
    {
      type: 'multiple-choice',
      description: 'Berapa hasil dari 3 x 5?',
      options: [
        '15',
        '10',
        '5'
      ],
      solution: {
        index: 0,
        text: '15'
      }
    },
    {
      type: 'multiple-answer',
      description: 'Operasi apa saja yang menghasilkan 15?',
      options: [
        '3 x 5',
        '2 x 5',
        '12 + 3'
      ],
      solutions: [
        {
          index: 0,
          text: '3 x 5'
        },
        {
          index: 2,
          text: '12 + 3'
        },
      ]
    },
    {
      type: 'short-text',
      description: 'Berapa 2 x 4?',
      solution: '8'
    },
  ]
}