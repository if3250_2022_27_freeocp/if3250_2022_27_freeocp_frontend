export const materialDataDummy = [
  {
    chapter_title: 'Bagian 1: Pendahuluan',
    content: [
      {
        type: 'article',
        title: 'Apa itu Assembly',
        link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
      },
      {
        type: 'video',
        title: 'Bahasa Assembly',
        link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
      },
      {
        type: 'assessment',
        title: 'Uji Pemahaman',
        id: ''
      },
    ]
  },
  {
    chapter_title: 'Bagian 2: Pengantar',
    content: [
      {
        type: 'article',
        title: 'Apa itu Assembly',
        link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
      },
      {
        type: 'video',
        title: 'Bahasa Assembly',
        link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
      },
    ]
  },
  {
    chapter_title: 'Bagian 3: Pembuka',
    content: [
      {
        type: 'article',
        title: 'Apa itu Assembly',
        link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
      },
      {
        type: 'video',
        title: 'Bahasa Assembly',
        link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
      },
    ]
  },
  {
    chapter_title: 'Bagian 4: Kesimpulan',
    content: [
      {
        type: 'article',
        title: 'Apa itu Assembly',
        link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
      },
      {
        type: 'video',
        title: 'Bahasa Assembly',
        link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
      },
    ]
  },
]