export const curationDataDummy = [
  {
    title: 'Bab Pengantar',
    course_title: 'Dasar Assembly',
    created_at: new Date(2022, 3, 1),
  },
  {
    title: 'Latihan Notasi Dasar',
    course_title: 'Pengenalan Algoritma Dasar',
    created_at: new Date(2022, 3, 2),
  },
  {
    title: 'Latihan Transpose Matriks',
    course_title: 'Aljabar dan Geometri',
    created_at: new Date(2022, 3, 3),
  },
  {
    title: 'Latihan 1',
    course_title: 'Statistika Lanjutan',
    created_at: new Date(2022, 2, 29),
  },
  {
    title: 'Latihan 1.1',
    course_title: 'Statistika Lanjutan',
    created_at: new Date(2022, 1, 11),
  },
]