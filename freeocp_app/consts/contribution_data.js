export const contributionDataDummy = [
  {
    thumbnail: 'https://atariwiki.org/wiki/attach/6502%20Assembly%20Code/6502%20Assembly%20Code%20in%20Sublime%20Text.jpg',
    title: 'Dasar Assembly',
    topic: 'Informatika',
    creator: 'Randy Zakya Suchrady Alter.',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
    created_at: new Date(2022, 1, 2),
    last_updated_at: new Date(2022, 1, 4),
    taken_count: 451
  },
]