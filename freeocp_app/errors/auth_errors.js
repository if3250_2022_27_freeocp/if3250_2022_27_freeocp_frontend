export const authErrors = {
    'Email is not valid': 'E-mail tidak valid',
    'Full name is not provided': 'Nama belum diisi',
    'Password must be at least 8 characters': 'Minimal panjang password 8 karakter',
    'Password must only consist of alphabets, underscore, numbers, hyphen, and period': 'Password hanya boleh terdiri dari alfabet, underscore, angka, hyphen, dan titik',
    'Password must consist of at least one number, one uppercase letter, and one lowercase letter': 'Password harus terdiri dari setidaknya satu angka, satu huruf besar, dan satu huruf kecil',
    'Password and password confirmation must be the same': 'Password dan konfirmasi password harus sama',
    'Email is already taken': 'E-mail telah diambil',
    "Email and password don't match": 'E-mail dan password tidak cocok',
    'Successfully created a new user': 'User baru telah dibuat, silahkan login',
    'User is now a curator': 'Kurator baru telah dibuat, silahkan login'
}
