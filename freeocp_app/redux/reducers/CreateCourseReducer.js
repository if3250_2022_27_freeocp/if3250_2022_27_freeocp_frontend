import { combineReducers } from 'redux'

const allowAssessmentReducer = (state = false, action) => {
  switch (action.type) {
    case 'CHANGE_ALLOW_ASSESSMENT':
      return action.payload
    default:
      return state
  }
}

const courseIdReducer = (state = '', action) => {
  switch (action.type) {
    case 'CHANGE_COURSE_ID':
      return action.payload
    default:
      return state
  }
}

const titleReducer = (state = '', action) => {
  switch (action.type) {
    case 'CHANGE_TITLE':
      return action.payload
    default:
      return state
  }
}

const topicReducer = (state = '', action) => {
  switch (action.type) {
    case 'CHANGE_TOPIC':
      return action.payload
    default:
      return state
  }
}

const descriptionReducer = (state = '', action) => {
  switch (action.type) {
    case 'CHANGE_DESC':
      return action.payload
    default:
      return state
  }
}

const syllabusReducer = (state = '', action) => {
  switch (action.type) {
    case 'CHANGE_SYLLABUS':
      return action.payload
    default:
      return state
  }
}

const materialListReducer = (state = [], action) => {
  switch (action.type) {
    case 'CHANGE_CHAPTER':
      return action.payload.newChapter
    case 'ADD_CHAPTER':
      state.push({
        chapter_title: '',
        content: []
      })
      return [...state]
    case 'DELETE_CHAPTER':
      state.splice(action.payload.chapterIdx, 1)
      return [...state]
    case 'CHANGE_CHAPTER_TITLE':
      state[action.payload.chapterIdx].chapter_title = action.payload.newChapterTitle
      return [...state]
    case 'ADD_CHAPTER_MATERIAL':
      state[action.payload.chapterIdx].content.push(action.payload.chapterMaterialData)
      return [...state]
    case 'CHANGE_CHAPTER_MATERIAL':
      state[action.payload.chapterIdx].content[action.payload.chapterMaterialIdx] = action.payload.newChapterMaterialData
      return [...state]
    case 'DELETE_CHAPTER_MATERIAL':
      state[action.payload.chapterIdx].content.splice(action.payload.chapterMaterialIdx, 1)
      return [...state]
    default:
      return [...state]
  }
}

export const CreateCourseReducer = combineReducers({
  allowAssessment: allowAssessmentReducer,
  courseId: courseIdReducer,
  title: titleReducer,
  topic: topicReducer,
  description: descriptionReducer,
  syllabus: syllabusReducer,
  materialList: materialListReducer
})

