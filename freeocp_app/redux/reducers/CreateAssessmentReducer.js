import { combineReducers } from 'redux'

const titleReducer = (state = '', action) => {
  switch (action.type) {
    case 'CHANGE_TITLE':
      return action.payload
    default:
      return state
  }
}

const questionsReducer = (state = [], action) => {
  switch (action.type) {
    case 'INIT_QUESTIONS_DATA':
      state = action.payload.questions
      return [...state]
    case 'ADD_QUESTION':
      if (action.payload.type === 'multiple-choice') {
        state.push({
          type: action.payload.type,
          description: '',
          options: [''],
          solution: {
            index: 0,
            text: ''
          }
        })
      } else if (action.payload.type === 'multiple-answer') {
        state.push({
          type: action.payload.type,
          description: '',
          options: [''],
          solutions: [{
            index: 0,
            text: ''
          }]
        })
      } else {
        state.push({
          type: action.payload.type,
          description: '',
          solution: ''
        })
      }
      return [...state]
    case 'DELETE_QUESTION':
      state.splice(action.payload.questionIdx, 1)
      return [...state]
    case 'CHANGE_QUESTION_DESCRIPTION':
      state[action.payload.questionIdx].description = action.payload.newQuestionDescription
      return [...state]
    case 'ADD_QUESTION_OPTION':
      state[action.payload.questionIdx].options.push('')
      return [...state]
    case 'CHANGE_QUESTION_OPTION':
      state[action.payload.questionIdx].options[action.payload.optionIdx] = action.payload.newOption
      if (action.payload.type === 'multiple-choice') {
        if (state[action.payload.questionIdx].solution.index === action.payload.optionIdx) {
          state[action.payload.questionIdx].solution.text = action.payload.newOption
        }
      } else if (action.payload.type === 'multiple-answer') {
        for (let i=0;i<state[action.payload.questionIdx].solutions.length;i++) {
          if (state[action.payload.questionIdx].solutions[i].index === action.payload.optionIdx) {
            state[action.payload.questionIdx].solutions[i].text = action.payload.newOption
            break
          }
        }
      }
      return [...state]
    case 'DELETE_QUESTION_OPTION':
      if (state[action.payload.questionIdx].type === 'multiple-choice' &&
      state[action.payload.questionIdx].solution.index === action.payload.optionIdx) {
        state[action.payload.questionIdx].solution = {
          index: 0,
          text: state[action.payload.questionIdx].options[0]
        }
      }
      if (state[action.payload.questionIdx].type === 'multiple-answer' &&
      state[action.payload.questionIdx].solutions.length === 1 &&
      state[action.payload.questionIdx].solutions[0].index === action.payload.optionIdx) {
        state[action.payload.questionIdx].solutions.push({
          index: 0,
          text: state[action.payload.questionIdx].options[0]
        })
      }
      state[action.payload.questionIdx].options.splice(action.payload.optionIdx, 1)
      return [...state]
    case 'CHANGE_MULTIPLE_CHOICE_SOLUTION':
      state[action.payload.questionIdx].solution = {
        index: action.payload.optionIdx,
        text: action.payload.optionText
      }
      return [...state]
    case 'ADD_MULTIPLE_ANSWER_SOLUTION':
      state[action.payload.questionIdx].solutions.push({
        index: action.payload.optionIdx,
        text: action.payload.optionText
      })
      return [...state]
    case 'DELETE_MULTIPLE_ANSWER_SOLUTION':
      state[action.payload.questionIdx].solutions.splice(action.payload.solutionIdx, 1)
      return [...state]
    case 'CHANGE_SHORT_TEXT_SOLUTION':
      state[action.payload.questionIdx].solution = action.payload.solution
      return [...state]
    default:
      return [...state]
  }
}

export const CreateAssessmentReducer = combineReducers({
  title: titleReducer,
  questions: questionsReducer
})

