import { combineReducers } from 'redux'

const questionsReducer = (state = [], action) => {
  switch (action.type) {
    case 'INIT_QUESTIONS_DATA':
      state = action.payload.questions
      // for (let question of action.payload.questions) {
      //   if (question.type === 'multiple-choice') {
      //     question.solution = {
      //       index: 0,
      //       text: question.options[0]
      //     }
      //   } else if (question.type === 'multiple-answer') {
      //     question.solutions = [{
      //       index: 0,
      //       text: question.options[0]
      //     }]
      //   } else {
      //     question.solution = ''
      //   }
      // }
      // console.log(state)
      return [...state]
    case 'CHANGE_MULTIPLE_CHOICE_SOLUTION':
      state[action.payload.questionIdx].solution = {
        index: action.payload.optionIdx,
        text: action.payload.optionText
      }
      return [...state]
    case 'ADD_MULTIPLE_ANSWER_SOLUTION':
      state[action.payload.questionIdx].solutions.push({
        index: action.payload.optionIdx,
        text: action.payload.optionText
      })
      return [...state]
    case 'DELETE_MULTIPLE_ANSWER_SOLUTION':
      state[action.payload.questionIdx].solutions.splice(action.payload.solutionIdx, 1)
      return [...state]
    case 'CHANGE_SHORT_TEXT_SOLUTION':
      state[action.payload.questionIdx].solution = action.payload.solution
      return [...state]
    default:
      return [...state]
  }
}

const takesAssessmentReducer = (state = {}, action) => {
  switch (action.type) {
    case 'INIT_TAKES_ASSESSMENT_DATA':
      state = action.payload.takesAssessment
      return {...state}
    default:
      return {...state}
  }
}

const isCurateReducer = (state = false, action) => {
  switch (action.type) {
    case 'SET_IS_CURATE':
      state = true
      return state
    default:
      return state
  }
}

export const ViewAssessmentScoreReducer = combineReducers({
  questions: questionsReducer,
  takesAssessment: takesAssessmentReducer,
  isCurate: isCurateReducer
})

