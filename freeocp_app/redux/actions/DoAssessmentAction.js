export const initQuestionsData = (questions) => {
  return {
    type: 'INIT_QUESTIONS_DATA',
    payload: {
      questions
    }
  }
}

export const initQuestionsDataCuration = (questions) => {
  return {
    type: 'INIT_QUESTIONS_DATA_CURATION',
    payload: {
      questions
    }
  }
}

export const setIsCurate = () => {
  return {
    type: 'SET_IS_CURATE',
  }
}

export const changeMultipleChoiceSolution = (questionIdx, optionIdx, optionText) => {
  return {
    type: 'CHANGE_MULTIPLE_CHOICE_SOLUTION',
    payload: {
      questionIdx,
      optionIdx,
      optionText
    }
  }
}

export const addMultipleAnswerSolution = (questionIdx, optionIdx, optionText) => {
  return {
    type: 'ADD_MULTIPLE_ANSWER_SOLUTION',
    payload: {
      questionIdx,
      optionIdx,
      optionText
    }
  }
}

export const deleteMultipleAnswerSolution = (questionIdx, solutionIdx) => {
  return {
    type: 'DELETE_MULTIPLE_ANSWER_SOLUTION',
    payload: {
      questionIdx,
      solutionIdx
    }
  }
}

export const changeShortTextSolution = (questionIdx, solution) => {
  return {
    type: 'CHANGE_SHORT_TEXT_SOLUTION',
    payload: {
      questionIdx,
      solution
    }
  }
}