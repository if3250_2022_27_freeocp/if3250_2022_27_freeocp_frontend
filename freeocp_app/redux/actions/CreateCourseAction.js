export const changeAllowAssessment = (value) => {
  return {
    type: 'CHANGE_ALLOW_ASSESSMENT',
    payload: value
  }
}

export const changeCourseId = (courseId) => {
  return {
    type: 'CHANGE_COURSE_ID',
    payload: courseId
  }
}

export const changeTitle = (newTitle) => {
  return {
    type: 'CHANGE_TITLE',
    payload: newTitle
  }
}

export const changeTopic = (newTopic) => {
  return {
    type: 'CHANGE_TOPIC',
    payload: newTopic
  }
}

export const changeDescription = (newDescription) => {
  return {
    type: 'CHANGE_DESC',
    payload: newDescription
  }
}

export const changeSyllabus = (newSyllabus) => {
  return {
    type: 'CHANGE_SYLLABUS',
    payload: newSyllabus
  }
}

export const changeChapter = (newChapter) => {
  return {
    type: 'CHANGE_CHAPTER',
    payload: {
      newChapter
    }
  }
}

export const addChapter = () => {
  return {
    type: 'ADD_CHAPTER',
  }
}

export const deleteChapter = (chapterIdx) => {
  return {
    type: 'DELETE_CHAPTER',
    payload: {
      chapterIdx
    }
  }
}

export const changeChapterTitle = (chapterIdx, newChapterTitle) => {
  return {
    type: 'CHANGE_CHAPTER_TITLE',
    payload: {
      chapterIdx,
      newChapterTitle
    }
  }
}

// {
//   type: 'material',
//   title: 'Apa itu Assembly',
//   link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
// },
// {
//   type: 'video',
//   title: 'Bahasa Assembly',
//   link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
// },
// {
//   type: 'assessment',
//   title: 'Uji Pemahaman',
//   id: ''
// },
export const addChapterMaterial = (chapterIdx, chapterMaterialData) => {
  return {
    type: 'ADD_CHAPTER_MATERIAL',
    payload: {
      chapterIdx,
      chapterMaterialData
    }
  }
}

export const changeChapterMaterial = (chapterIdx, chapterMaterialIdx, newChapterMaterialData) => {
  return {
    type: 'CHANGE_CHAPTER_MATERIAL',
    payload: {
      chapterIdx,
      chapterMaterialIdx,
      newChapterMaterialData
    }
  }
}

export const deleteChapterMaterial = (chapterIdx, chapterMaterialIdx) => {
  return {
    type: 'DELETE_CHAPTER_MATERIAL',
    payload: {
      chapterIdx,
      chapterMaterialIdx,
    }
  }
}
