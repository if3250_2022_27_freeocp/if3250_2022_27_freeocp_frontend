export const changeTitle = (newTitle) => {
  return {
    type: 'CHANGE_TITLE',
    payload: newTitle
  }
}

export const initQuestionsData = (questions) => {
  return {
    type: 'INIT_QUESTIONS_DATA',
    payload: {
      questions
    }
  }
}

export const addQuestion = (type) => {
  return {
    type: 'ADD_QUESTION',
    payload: {
      type
    }
  }
}

export const deleteQuestion = (questionIdx) => {
  return {
    type: 'DELETE_QUESTION',
    payload: {
      questionIdx
    }
  }
}

export const changeQuestionDescription = (questionIdx, newQuestionDescription) => {
  return {
    type: 'CHANGE_QUESTION_DESCRIPTION',
    payload: {
      questionIdx,
      newQuestionDescription
    }
  }
}

export const addQuestionOption = (questionIdx) => {
  return {
    type: 'ADD_QUESTION_OPTION',
    payload: {
      questionIdx
    }
  }
}

export const changeQuestionOption = (questionIdx, optionIdx, newOption, type) => {
  return {
    type: 'CHANGE_QUESTION_OPTION',
    payload: {
      questionIdx,
      optionIdx,
      newOption,
      type
    }
  }
}

export const deleteQuestionOption = (questionIdx, optionIdx) => {
  return {
    type: 'DELETE_QUESTION_OPTION',
    payload: {
      questionIdx,
      optionIdx,
    }
  }
}

export const changeMultipleChoiceSolution = (questionIdx, optionIdx, optionText) => {
  return {
    type: 'CHANGE_MULTIPLE_CHOICE_SOLUTION',
    payload: {
      questionIdx,
      optionIdx,
      optionText
    }
  }
}

export const addMultipleAnswerSolution = (questionIdx, optionIdx, optionText) => {
  return {
    type: 'ADD_MULTIPLE_ANSWER_SOLUTION',
    payload: {
      questionIdx,
      optionIdx,
      optionText
    }
  }
}

export const deleteMultipleAnswerSolution = (questionIdx, solutionIdx) => {
  return {
    type: 'DELETE_MULTIPLE_ANSWER_SOLUTION',
    payload: {
      questionIdx,
      solutionIdx
    }
  }
}

export const changeShortTextSolution = (questionIdx, solution) => {
  return {
    type: 'CHANGE_SHORT_TEXT_SOLUTION',
    payload: {
      questionIdx,
      solution
    }
  }
}