export const initQuestionsData = (questions) => {
  return {
    type: 'INIT_QUESTIONS_DATA',
    payload: {
      questions
    }
  }
}

export const initTakesAssessmentData = (takesAssessment) => {
  return {
    type: 'INIT_TAKES_ASSESSMENT_DATA',
    payload: {
      takesAssessment
    }
  }
}