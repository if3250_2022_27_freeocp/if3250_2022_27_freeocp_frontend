import React from "react";
import Head from "next/head";
import Navbar from "./components/navigations/Navbar";
import styles from "../styles/Profile.module.css";
import BlackOutlinedButton from "./components/buttons/BlackOutlinedButton";
import PrimaryDropdown from "./components/dropdowns/PrimaryDropdown";
import PrimaryPagination from "./components/paginations/PrimaryPagination";
import HistoryCard from "./components/cards/HistoryCard";
import ContributionCard from "./components/cards/ContributionCard";
import { contributionDataDummy } from "../consts/contribution_data";
import { getProfile, getCoursesProgress, getCoursesPublished } from "./api/profile";
import PrimaryProfileModal from "./components/modals/profile/PrimaryProfileModal";
import Footer from "./components/navigations/Footer";

export default function Profile() {
  // profile data
  const [fullName, setFullname] = React.useState('')
  const [institution, setInstitution] = React.useState('')
  const [description, setDescription] = React.useState('')

  // sections, paginations, and sorts
  const [currentSection, setCurrentSection] = React.useState(0)
  const [currentSortByIdHistory, setCurrentSortByIdHistory] = React.useState(0)
  const [currentSortTypeIdHistory, setCurrentSortTypeIdHistory] = React.useState(0)
  const [currentSortByIdContribution, setCurrentSortByIdContribution] = React.useState(0)
  const [currentSortTypeIdContribution, setCurrentSortTypeIdContribution] = React.useState(0)
  const [currentHistoryData, setCurrentHistoryData] = React.useState([])
  const [currentContributionData, setCurrentContributionData] = React.useState(contributionDataDummy)

  // modals
  const [changeNameModalVisible, setChangeNameModalVisible] = React.useState(false)
  const [changeInstitutionModalVisible, setChangeInstitutionModalVisible] = React.useState(false)
  const [changeDescriptionModalVisible, setChangeDescriptionModalVisible] = React.useState(false)

  const sortByHistory = [
    {
      id: 0,
      text: 'Nama',
      onClick: () => {setCurrentSortByIdHistory(0)}
    },
    {
      id: 1,
      text: 'Topik',
      onClick: () => {setCurrentSortByIdHistory(1)}
    },
    {
      id: 2,
      text: 'Kemajuan',
      onClick: () => {setCurrentSortByIdHistory(2)}
    },
    {
      id: 3,
      text: 'Tanggal Mulai',
      onClick: () => {setCurrentSortByIdHistory(3)}
    },
  ]
  const sortByContribution = [
    {
      id: 0,
      text: 'Nama',
      onClick: () => {setCurrentSortByIdContribution(0)}
    },
    {
      id: 1,
      text: 'Topik',
      onClick: () => {setCurrentSortByIdContribution(1)}
    },
    {
      id: 2,
      text: 'Dipelajari',
      onClick: () => {setCurrentSortByIdContribution(2)}
    },
    {
      id: 3,
      text: 'Tanggal',
      onClick: () => {setCurrentSortByIdContribution(3)}
    },
  ]
  const sortTypeHistory = [
    {
      id: 0,
      text: 'Menaik',
      onClick: () => {setCurrentSortTypeIdHistory(0)}
    },
    {
      id: 1,
      text: 'Menurun',
      onClick: () => {setCurrentSortTypeIdHistory(1)}
    }
  ]
  const sortTypeContribution = [
    {
      id: 0,
      text: 'Menaik',
      onClick: () => {setCurrentSortTypeIdContribution(0)}
    },
    {
      id: 1,
      text: 'Menurun',
      onClick: () => {setCurrentSortTypeIdContribution(1)}
    }
  ]
  const sortHistoryData = () => {
    let tempData = currentHistoryData
    let tempSortBy = ''
    if (currentSortByIdHistory == 0) {
      tempSortBy = 'title'
    } else if (currentSortByIdHistory == 1) {
      tempSortBy = 'topic'
    } else if (currentSortByIdHistory == 2) {
      tempSortBy = 'progress'
    } else {
      tempSortBy = 'start_at'
    }
    tempData.sort(function(a, b) {
      if (currentSortTypeIdHistory == 0) {
        if (typeof a[tempSortBy] === 'string' || a[tempSortBy] instanceof String) {
          return a[tempSortBy].localeCompare(b[tempSortBy])
        } else {
          return a[tempSortBy] - b[tempSortBy]
        }
      } else {
        if (typeof a[tempSortBy] === 'string' || a[tempSortBy] instanceof String) {
          return b[tempSortBy].localeCompare(a[tempSortBy])
        } else {
          return b[tempSortBy] - a[tempSortBy]
        }
      }
    })
    setCurrentHistoryData([...tempData])
  }
  const sortContributionData = () => {
    let tempData = currentContributionData
    let tempSortBy = ''
    if (currentSortByIdContribution == 0) {
      tempSortBy = 'title'
    } else if (currentSortByIdContribution == 1) {
      tempSortBy = 'topic'
    } else if (currentSortByIdContribution == 2) {
      tempSortBy = 'taken_count'
    } else {
      tempSortBy = 'created_at'
    }
    tempData.sort(function(a, b) {
      if (currentSortTypeIdContribution == 0) {
        if (typeof a[tempSortBy] === 'string' || a[tempSortBy] instanceof String) {
          return a[tempSortBy].localeCompare(b[tempSortBy])
        } else {
          return a[tempSortBy] - b[tempSortBy]
        }
      } else {
        if (typeof a[tempSortBy] === 'string' || a[tempSortBy] instanceof String) {
          return b[tempSortBy].localeCompare(a[tempSortBy])
        } else {
          return b[tempSortBy] - a[tempSortBy]
        }
      }
    })
    setCurrentContributionData([...tempData])
  }
  React.useEffect(() => {
    getProfile().then(function(res) {
      const { date_of_birth, description, email, full_name, institution, profile_picture } = res.data
      setFullname(full_name)
      setInstitution(institution)
      setDescription(description)
    }).catch(function(err) {
      console.log(err)
    })
    getCoursesProgress().then(function(res) {
      setCurrentHistoryData([...res.data])
    }).catch(function(err) {
      console.log(err)
    })
    getCoursesPublished().then(function(res) {
      setCurrentContributionData([...res.data])
    }).catch(function(err) {
      console.log(err)
    })
  }, [])
  React.useEffect(() => {
    sortHistoryData()
  }, [currentSortByIdHistory, currentSortTypeIdHistory])
  React.useEffect(() => {
    sortContributionData()
  }, [currentSortByIdContribution, currentSortTypeIdContribution])
  const profileSection = () => {
    return (
      <div className='mb-16'>
        <p>
          <span className='text-2xl font-bold mr-3'>Profil</span>
          <span 
            className='gray-text underline mt-2 text-sm cursor-pointer whitespace-nowrap'
            onClick={() => setChangeDescriptionModalVisible(true)}
          >
            Ubah Deskripsi Profil
          </span>
        </p>
        {description !== ''?
          <p className='mt-4'>
            {description}
          </p>
        :
          <p className='blue-text mt-4 text-lg font-medium'>Tidak ada deskripsi profil</p>
        }
      </div>
    )
  }
  const historySection = () => {
    return (
      <div className='mb-16'>
        <p>
          <span className='text-2xl font-bold mr-3'>Jejak Belajar</span>
        </p>
        {currentHistoryData.length > 0 ?
          <>
            <div className='mt-4 flex justify-between'>
              <PrimaryPagination pages={Math.ceil(currentHistoryData.length/8)} />
              <div className='flex'>
                <PrimaryDropdown label='Urut berdasarkan:' className='mr-6' options={sortByHistory} />
                <PrimaryDropdown label='Pengurutan:' options={sortTypeHistory} />
              </div>
            </div>
            <div className='mt-4 grid gap-x-3 gap-y-4 grid-cols-3'>
              {currentHistoryData.map((historyData) => (
                <HistoryCard historyData={historyData} />
              ))}
            </div>
          </>
        :
          <p className='blue-text mt-4 text-lg font-medium'>Tidak ada hasil yang ditemukan</p>
        }
      </div>
    )
  }
  const contributionSection = () => {
    return (
      <div className='mb-16'>
        <p>
          <span className='text-2xl font-bold mr-3'>Kontribusi Ilmu</span>
        </p>
        {currentContributionData.length > 0 ?
          <>
            <div className='mt-4 flex justify-between'>
              <PrimaryPagination pages={Math.ceil(currentContributionData.length/8)} />
              <div className='flex'>
                <PrimaryDropdown label='Urut berdasarkan:' className='mr-6' options={sortByContribution} />
                <PrimaryDropdown label='Pengurutan:' options={sortTypeContribution} />
              </div>
            </div>
            <div className='mt-4 grid gap-x-3 gap-y-4 grid-cols-3'>
              {currentContributionData.map((contributionData) => (
                <ContributionCard contributionData={contributionData} />
              ))}
            </div>
          </>
        :
          <p className='blue-text mt-4 text-lg font-medium'>Tidak ada hasil yang ditemukan</p>
        }
      </div>
    )
  }
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500&display=swap" rel="stylesheet" />
      </Head>

      <div className='navbar-margin'>
        <Navbar spaceBottom />
      </div>

      <div style={{ minHeight: 'calc(100vh - 140px - 71px)' }}>
        <div className='w-full'>
          <PrimaryProfileModal 
            visible={changeNameModalVisible} 
            setVisible={setChangeNameModalVisible}
            title='Tampilan Nama'
            initialValue={fullName}
            setFinalValue={setFullname}
            type='full_name'
          />
          <PrimaryProfileModal 
            visible={changeInstitutionModalVisible} 
            setVisible={setChangeInstitutionModalVisible}
            title='Nama Institusi / Akademisi'
            initialValue={institution}
            setFinalValue={setInstitution}
            type='institution'
          />
          <PrimaryProfileModal 
            visible={changeDescriptionModalVisible} 
            setVisible={setChangeDescriptionModalVisible}
            title='Deskripsi Profil'
            initialValue={description}
            setFinalValue={setDescription}
            textArea
            type='description'
          />

          <div>
            <div className={['', styles.themeHead].join(' ')}>
              
            </div>
          </div>

          <div>
            <div className={['px-56 flex'].join(' ')}>
              <div className={['flex flex-col items-center mr-10', styles.leftContainer].join(' ')}>
                <img src='profile/avatar.png' />
                <p className='gray-text underline mt-2 text-sm cursor-pointer whitespace-nowrap'>Ubah Foto Profil & Banner</p>
                <BlackOutlinedButton 
                  blueHover 
                  selected={currentSection === 0} 
                  text='Profil' 
                  className='w-44 mt-4' 
                  onClick={() => setCurrentSection(0)} 
                />
                <BlackOutlinedButton 
                  blueHover 
                  selected={currentSection === 1} 
                  text='Jejak Belajar' 
                  className='w-44 mt-2' 
                  onClick={() => setCurrentSection(1)} 
                />
                <BlackOutlinedButton 
                  blueHover 
                  selected={currentSection === 2} 
                  text='Kontribusi Ilmu' 
                  className='w-44 px-0 mt-2' 
                  style={{ paddingLeft: 0, paddingRight: 0 }} 
                  onClick={() => setCurrentSection(2)} 
                />
              </div>
              <div className={['flex flex-col mt-4 grow'].join(' ')}>
                <p>
                  <span className='text-3xl mr-3'>{fullName}</span>
                  <span 
                    className='gray-text underline mt-2 text-sm cursor-pointer whitespace-nowrap'
                    onClick={() => setChangeNameModalVisible(true)}
                  >
                    Ubah Tampilan Nama
                  </span>
                </p>
                <p>
                  <span className='text-xl mr-3 blue-text italic'>{institution}</span>
                  <span 
                    className='gray-text underline mt-2 text-sm cursor-pointer whitespace-nowrap'
                    onClick={() => setChangeInstitutionModalVisible(true)}
                  >
                    Ubah Institusi
                  </span>
                </p>
                <div class="py-4">
                    <div class="w-full border-t border-gray-700"></div>
                </div>
                {currentSection === 0 && profileSection()}
                {currentSection === 1 && historySection()}
                {currentSection === 2 && contributionSection()}
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
}