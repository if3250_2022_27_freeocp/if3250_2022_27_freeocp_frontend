import { axiosClient } from './client'

export function login(email, password) {
    let data = { email, password }
    return axiosClient.post('/login', JSON.stringify(data))
}

export function register(email, full_name, password, password_confirmation) {
    let data = { email, full_name, password, password_confirmation }
    return axiosClient.post('/register', JSON.stringify(data))
}

export function isUserAuth() {
    return axiosClient.get('/is-auth', {
        headers: {
          "x-access-token": localStorage.getItem('token')
        }
      })
}

export function logout() {
    return axiosClient.post('/logout')
}
