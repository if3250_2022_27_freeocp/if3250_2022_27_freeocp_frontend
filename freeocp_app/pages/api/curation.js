import { interceptedAxiosClient } from './client'

export function getAllPendingAssessments(page) {
    return interceptedAxiosClient.get(`/curator/all-pending/${page}`, {
      headers: {
        "x-access-token": localStorage.getItem('token')
      }
    })
}

export function reviewAssessment(assessment_id) {
    return interceptedAxiosClient.put(`/curator/review/${assessment_id}`, JSON.stringify({}), {
      headers: {
        "x-access-token": localStorage.getItem('token')
      }
    })
}

export function acceptAssessment(assessment_id) {
  return interceptedAxiosClient.put(`/curator/accept/${assessment_id}`, JSON.stringify({}), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function declineAssessment(assessment_id) {
  return interceptedAxiosClient.put(`/curator/decline/${assessment_id}`, JSON.stringify({}), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getAssessmentInReviewByCuratorId() {
  return interceptedAxiosClient.get('/curator/review', {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getAssessmentAcceptedByCuratorId() {
  return interceptedAxiosClient.get('/curator/accept', {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getAssessmentDeclinedByCuratorId() {
  return interceptedAxiosClient.get('/curator/decline', {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getAssessmentByCuratorId(page) {
  return interceptedAxiosClient.get(`/curator/assessments/all/${page}`, {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function becomeACurator(agree_statement) {
  return interceptedAxiosClient.put(`/curator/become-a-curator`, JSON.stringify({ agree_statement }), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}
