import { axiosClient } from './client'

export function getCourses() {
  return axiosClient.get('/search')
}

export function getCoursesByQueryRegex(query, page) {
    return axiosClient.get(`/search/regex/${query}/${page}`)
}

export function getCoursesByQueryCosine(query, page) {
  return axiosClient.get(`/search/${query}/${page}`)
}