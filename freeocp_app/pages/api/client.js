import axios from 'axios'

const devURL = 'http://localhost:8000'
const stagingURL = 'https://quiet-thicket-68195.herokuapp.com/'

const selectedURL = devURL

export const axiosClient = axios.create({
  baseURL: selectedURL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
})

export const interceptedAxiosClient = axios.create({
  baseURL: selectedURL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
})

interceptedAxiosClient.interceptors.response.use (
  function (response) {
    return response;
  }, 
  function (error) {
    let res = error.response;
    if (res.status == 401) {
      window.location.href = '/login';
    }
  }
)