import { interceptedAxiosClient } from './client'

export function accomplishMaterial(courseId, materialId) {
  return interceptedAxiosClient.post(`/grading/${courseId}/material/${materialId}`, JSON.stringify({}), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function accomplishAssessment(courseId, assessmentId, answers) {
  return interceptedAxiosClient.post(`/grading/${courseId}/assessment/${assessmentId}`, JSON.stringify({answers}), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}