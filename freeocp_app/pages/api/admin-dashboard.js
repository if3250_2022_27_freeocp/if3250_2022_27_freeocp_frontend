import { interceptedAxiosClient } from './client'

export function getAllCuratorRegistrations(page) {
  return interceptedAxiosClient.get(`/admin-dashboard/all/${page}`, {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getPendingCuratorRegistrations(page) {
  return interceptedAxiosClient.get(`/admin-dashboard/pending/${page}`, {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function approveCuratorRegistration(curator_registration_id) {
  return interceptedAxiosClient.put(`/admin-dashboard/approve/${curator_registration_id}`, JSON.stringify({}), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function rejectCuratorRegistration(curator_registration_id) {
  return interceptedAxiosClient.put(`/admin-dashboard/reject/${curator_registration_id}`, JSON.stringify({}), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}