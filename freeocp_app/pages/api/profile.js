import { interceptedAxiosClient } from './client'

export function getProfile() {
    return interceptedAxiosClient.get('/user', {
      headers: {
        "x-access-token": localStorage.getItem('token')
      }
    })
}

export function updateProfile(params) {
    // params = { full_name, date_of_birth, profile_picture, institution, description }
    return interceptedAxiosClient.post('/user', JSON.stringify(params), {
      headers: {
        "x-access-token": localStorage.getItem('token')
      }
    })
}

export function changePassword(new_password) {
  let data = { new_password }
  return interceptedAxiosClient.post('/user/change-password', JSON.stringify(data), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getCoursesProgress() {
  return interceptedAxiosClient.get('/user/courses', {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getCoursesPublished() {
  return interceptedAxiosClient.get('/user/courses/published', {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}