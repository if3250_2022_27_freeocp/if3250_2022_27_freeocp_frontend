import { interceptedAxiosClient } from './client'

export function getPopularCourse() {
  return interceptedAxiosClient.get(`/course/popular`, {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getCourse(course_id) {
    return interceptedAxiosClient.get(`/course/${course_id}`, {
      headers: {
        "x-access-token": localStorage.getItem('token')
      }
    })
}

export function enrollCourse(course_id) {
  return interceptedAxiosClient.post(`/enroll/${course_id}`, JSON.stringify({}), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function updateCourse(course_id,title,topic,thumbnail,description,syllabus,chapters_id,materials) {
  let params = {title,topic,thumbnail,description,syllabus,chapters_id,materials}
  return interceptedAxiosClient.put(`/course-management/course/${course_id}/update`, JSON.stringify(params), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function createCourse(title, topic, thumbnail, description, syllabus, chapters_id, materials) {
    let params = { title, topic, thumbnail, description, syllabus, chapters_id, materials }
    return interceptedAxiosClient.post('/course-management/course/create', JSON.stringify(params), {
      headers: {
        "x-access-token": localStorage.getItem('token')
      }
    })
}

export function createChapter(title, materials_id) {
  let params = { title, materials_id }
  return interceptedAxiosClient.post('/course-management/chapter/create', JSON.stringify(params), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function deleteChapter(course_id, chapter_id) {
  return interceptedAxiosClient.delete(`/course-management/course/${course_id}/chapter/${chapter_id}/delete`, {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function createMaterial(title, type, link) {
  let params = { title, type, link }
  return interceptedAxiosClient.post('/course-management/material/create', JSON.stringify(params), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}
