import { interceptedAxiosClient } from './client'

export function createQuestion(type, description, options, solution) {
    let params = { type, description, options, solution }
    return interceptedAxiosClient.post('/assessment/question/create', JSON.stringify(params), {
      headers: {
        "x-access-token": localStorage.getItem('token')
      }
    })
}

export function createAssessment(title, questions_id) {
  let params = { title, questions_id }
  return interceptedAxiosClient.post('/assessment/create', JSON.stringify(params), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function getAssessment(assessmentId) {
  return interceptedAxiosClient.get(`/assessment/${assessmentId}`, {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function updateQuestion(assessment_id, question_id, type, description, options, solution) {
  let params = { type, description, options, solution }
  return interceptedAxiosClient.put(`/assessment/${assessment_id}/question/${question_id}/update`, JSON.stringify(params), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function updateAssessment(assessment_id, title, questions_id) {
  let params = { title, questions_id }
  return interceptedAxiosClient.put(`/assessment/${assessment_id}/update`, JSON.stringify(params), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function addAssessmentToChapter(course_id, chapter_id, assessment_id) {
  let params = { assessment_id }
  return interceptedAxiosClient.put(`/course-management/course/${course_id}/chapter/${chapter_id}/add-assessment`, JSON.stringify(params), {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}

export function deleteQuestion(assessment_id, question_id) {
  return interceptedAxiosClient.delete(`/assessment/${assessment_id}/question/${question_id}/delete`, {
    headers: {
      "x-access-token": localStorage.getItem('token')
    }
  })
}