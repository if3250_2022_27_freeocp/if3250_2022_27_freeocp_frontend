import React from 'react'
import Head from 'next/head'
import Navbar from './components/navigations/Navbar'
import styles from '../styles/Curations.module.css'
import PrimaryPagination from './components/paginations/PrimaryPagination'
import PrimaryDropdown from './components/dropdowns/PrimaryDropdown'
import SecondaryButton from './components/buttons/SecondaryButton'
import SecondaryDangerButton from './components/buttons/SecondaryDangerButton'
import CourseCard from './components/cards/CourseCard'
import { getCoursesByQueryCosine } from './api/search'
import Footer from './components/navigations/Footer'
import { curationDataDummy } from '../consts/curation_data'
import { getAllPendingAssessments, reviewAssessment } from './api/curation'
import { getPendingCuratorRegistrations, approveCuratorRegistration, rejectCuratorRegistration } from './api/admin-dashboard'
import ConfirmModal from './components/modals/ConfirmModal'
import Router, { useRouter } from 'next/router'

export default function AddDashboard() {
  const router = useRouter()
  const [name, setName] = React.useState('')
  const [showModal, setShowModal] = React.useState(false)
  const [actionType, setActionType] = React.useState('decline')
  const [selectedCurationRegistration, setSelectedCurationRegistration] = React.useState({})
  const [currentSortById, setCurrentSortById] = React.useState(0)
  const [currentSortTypeId, setCurrentSortTypeId] = React.useState(0)
  const [currentCurationRegistrations, setCurrentCurationRegistrations] = React.useState([])
  const [trigger, setTrigger] = React.useState(false)
  const [count, setCount] = React.useState(0)
  const sortBy = [
    {
      id: 0,
      text: 'Email',
      onClick: () => {setCurrentSortById(0)}
    },
    {
      id: 1,
      text: 'Nama',
      onClick: () => {setCurrentSortById(1)}
    },
    {
      id: 2,
      text: 'Tanggal',
      onClick: () => {setCurrentSortById(2)}
    },
  ]
  const sortType = [
    {
      id: 0,
      text: 'Menaik',
      onClick: () => {setCurrentSortTypeId(0)}
    },
    {
      id: 1,
      text: 'Menurun',
      onClick: () => {setCurrentSortTypeId(1)}
    }
  ]
  const sortCourseData = () => {
    if (currentCurationRegistrations.length > 0) {
      let tempData = currentCurationRegistrations
      let isUserInfo = false
      let tempSortBy = ''
      if (currentSortById == 0) {
        tempSortBy = 'email'
        isUserInfo = true
      } else if (currentSortById == 1) {
        tempSortBy = 'full_name'
        isUserInfo = true
      } else {
        tempSortBy = 'submission_date'
        isUserInfo = false
      }
      tempData.sort(function(a, b) {
        if (isUserInfo) {
          a = a['user_info']
        }
        if (currentSortTypeId == 0) {
          if (typeof a[tempSortBy] === 'string' || a[tempSortBy] instanceof String) {
            return a[tempSortBy].localeCompare(b[tempSortBy])
          } else {
            return a[tempSortBy] - b[tempSortBy]
          }
        } else {
          if (typeof a[tempSortBy] === 'string' || a[tempSortBy] instanceof String) {
            return b[tempSortBy].localeCompare(a[tempSortBy])
          } else {
            return b[tempSortBy] - a[tempSortBy]
          }
        }
      })
      setCurrentCurationRegistrations([...tempData])
    }
  }
  const dateOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
  const handleOnItemClicked = (page) => {
    Router.push({
      pathname: '/curation',
      query: {
        page: page
      }
    })
  }
  React.useEffect(() => {
    let page = router.query.page
    getPendingCuratorRegistrations(page).then(function(res) {
      setCount(res.data.count)
      setCurrentCurationRegistrations(res.data.curator_registrations)
      console.log(res.data)
      setTrigger(!trigger)
    }).catch(function(err) {
      console.log(err)
    })
  }, [router])
  React.useEffect(() => {
    sortCourseData()
  }, [trigger])
  React.useEffect(() => {
    sortCourseData()
  }, [currentSortById, currentSortTypeId])
  const handleModalContent = () => {
    if (actionType === "decline") {
      return (
        <span>
          Apakah Anda yakin untuk <span className='red-text'>menolak</span> 
          &nbsp;{selectedCurationRegistration.user_info ? selectedCurationRegistration.user_info.full_name : ''}?
        </span>
      )
    } else {
      return (
        <span>
          Apakah Anda yakin untuk <span className='blue-text'>menerima</span>
          &nbsp;{selectedCurationRegistration.user_info ? selectedCurationRegistration.user_info.full_name : ''}?
        </span>
      )
    }
  }
  const handleModalOnConfirm = () => {
    if (actionType === "decline") {
      return () => {
        rejectCuratorRegistration(selectedCurationRegistration.curator_registration_id).then(function(res) {
          setShowModal(false)
          Router.push({
            pathname: 'admin-dashboard',
            query: {
              page: 1
            }
          })
        }).catch(function(err) {
          console.log(err)
        })
      }
    } else {
      return () => {
        approveCuratorRegistration(selectedCurationRegistration.curator_registration_id).then(function(res) {
          setShowModal(false)
          Router.push({
            pathname: 'admin-dashboard',
            query: {
              page: 1
            }
          })
        }).catch(function(err) {
          console.log(err)
        })
      }
    }
  }
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500&display=swap" rel="stylesheet" />
      </Head>

      <div className='navbar-margin'>
        <Navbar spaceBottom />
      </div>

      <ConfirmModal 
        visible={showModal}
        setVisible={setShowModal}
        onConfirm={handleModalOnConfirm()}
      >
        {handleModalContent()}
      </ConfirmModal>

      <div style={{ minHeight: 'calc(100vh - 140px - 71px)' }}>
        <div className='w-full'>
          <div className=''>
            <div className={['py-8 px-56', styles.searchTitle].join(' ')}>
              <span className='text-lg'>Daftar Pengguna Aktif Mendaftar Kurator</span><br />
              <span className='blue-text'>Ditemukan {count} Pengguna</span>
            </div>
          </div>

          {count > 0 ?
            <>
              <div className='px-56 mt-4 flex justify-between'>
                <PrimaryPagination pages={Math.ceil(count/8)} onItemClicked={handleOnItemClicked} />
                <div className='flex'>
                  <PrimaryDropdown label='Urut berdasarkan:' className='mr-6' options={sortBy} styleWidth='170' />
                  <PrimaryDropdown label='Pengurutan:' options={sortType} />
                </div>
              </div>

              <div className='px-56 mt-6 mb-16'>
                <table className="table-fixed w-full">
                  <thead>
                    <tr className={['', styles.row].join(' ')}>
                      <th className='text-left py-2 w-3/12'>Email</th>
                      <th className='text-left py-2 w-3/12'>Nama</th>
                      <th className='py-2 w-1/6'>Tanggal Pengajuan</th>
                      <th className='w-1/6'></th>
                      <th className='w-1/6'></th>
                    </tr>
                  </thead>
                  <tbody>
                    {currentCurationRegistrations.map((registrationData) => (
                      <tr className={['', styles.row].join(' ')}>
                        <td className='py-2 w-1/3 pr-2 blue-text'>{registrationData.user_info.email}</td>
                        <td className='py-2 w-1/3 pr-2'>{registrationData.user_info.full_name}</td>
                        <td className='py-2 text-center w-1/6 px-1'>{new Date(registrationData.submission_date).toLocaleDateString("id-ID", dateOptions)}</td>
                        <td className='px-1'>
                          <SecondaryDangerButton 
                            className='w-full my-2' 
                            text="Tolak" 
                            onClick={() => {
                              setActionType('decline')
                              setSelectedCurationRegistration(registrationData)
                              setShowModal(true)
                            }}
                          />
                        </td>
                        <td className='px-1'>
                          <SecondaryButton 
                            className='w-full my-2' 
                            text="Terima" 
                            onClick={() => {
                              setActionType('accept')
                              setSelectedCurationRegistration(registrationData)
                              setShowModal(true)
                            }}
                          />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </>
          :
            <p className='blue-text px-56 mt-4 text-xl font-medium'>Tidak ada hasil yang ditemukan</p>
          }
        </div>
      </div>

      <Footer />
    </div>
  )
}
