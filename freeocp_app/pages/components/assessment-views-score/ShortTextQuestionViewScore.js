import styles from '../../../styles/Buttons.module.css';
import SectionContainer from '../containers/SectionContainer';
import { useSelector, useDispatch } from "react-redux";
import { changeShortTextSolution } from '../../../redux/actions/CreateAssessmentAction';

const ShortTextQuestionViewScore = ({ questionIdx }) => {
	if (questionIdx == null) {
    return null
  }
	const dispatch = useDispatch()
	const description = useSelector(state => state.questions[questionIdx].description)
	const solution = useSelector(state => state.questions[questionIdx].solution)
	const isCurate = useSelector(state => state.isCurate)
	return (
		<div className='my-6 flex items-start'>
			<SectionContainer title={`${questionIdx + 1} (Isian)`}>
				<div className='py-6 px-12'>
					<div className='flex flex-row rounded-3xl w-full grow py-1'>
						<p className='text-xl mb-2'>{description}</p>
					</div>
          <div className='flex flex-row rounded-3xl w-full grow px-3 py-1 border-gray-800' style={{ borderWidth: '1px' }}>
            <input 
              className="appearance-none py-1 text-xl w-full mx-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
              type="text" 
              placeholder="Tuliskan jawaban Anda..."
              value={solution}
              onChange={(e) => dispatch(changeShortTextSolution(questionIdx, e.target.value))}
							disabled={isCurate}
            ></input>
          </div>
				</div>
			</SectionContainer>
		</div>
	)
}

export default ShortTextQuestionViewScore;