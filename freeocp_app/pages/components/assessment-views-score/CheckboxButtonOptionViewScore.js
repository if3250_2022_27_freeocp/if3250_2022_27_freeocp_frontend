import React from 'react';
import styles from '../../../styles/Assessments.module.css';
import SectionContainer from '../containers/SectionContainer';
import { useSelector, useDispatch } from "react-redux";
import { addMultipleAnswerSolution, deleteMultipleAnswerSolution, changeQuestionOption, deleteQuestionOption } from '../../../redux/actions/DoAssessmentAction';

const CheckboxButtonOptionViewScore = ({ optionIdx=null, name=null }) => {
  if (optionIdx == null || name == null) {
    return null
  }
	const dispatch = useDispatch()
  const options = useSelector(state => state.questions[name].options)
  const takesAssessment = useSelector(state => state.takesAssessment)
  const solutions = useSelector(state => state.questions[name].solutions)
  const isOptionChecked = () => {
    let found = false
    let solutionIndex = -1
    for (let i=0;i<solutions.length;i++) {
      if (solutions[i].index === optionIdx) {
        found = true
        solutionIndex = i
        break
      }
    }
    return {
      found,
      solutionIndex
    }
  }
  return (
    <div className='flex my-3'>
      <div 
        className={['flex flex-row rounded-3xl w-full grow border-gray-800 items-center',
        (options[optionIdx].text === '' || !options[optionIdx].text) && 'h-12', 
        solutions.find((solution) => {
          return solution.index === optionIdx
        }) && styles.curateSolution].join(' ')}
        style={{ borderWidth: '1px' }}
      >
        <input 
          type="checkbox" 
          checked={takesAssessment.answers ? takesAssessment.answers[name].answer_index.find((answer) => {
            return answer === optionIdx
          }) : false}
          name={name}
          className='ml-3 mr-2 cursor-pointer form-check-input'
          readOnly
        ></input>
        <div className={[styles.optionAlphabet, 'w-10 mr-2 flex items-center justify-center'].join(' ')}>
          {String.fromCharCode(optionIdx + 65)}.
        </div>
        <p 
          className="py-2 text-lg w-full mr-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
        >
          {options[optionIdx].text && options[optionIdx].text !== '' ? options[optionIdx].text : options[optionIdx]}
        </p>
      </div>
    </div>
	)
}

export default CheckboxButtonOptionViewScore;