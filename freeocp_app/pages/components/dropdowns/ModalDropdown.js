import React from 'react';
import styles from '../../../styles/Buttons.module.css';

const ModalDropdown = ({ className, label, options=[], materialData=null }) => {
  if (materialData == null) {
    return null
  }
  const [openDropdown, setOpenDropdown] = React.useState(false)
  const [currentOption, setCurrentOption] = React.useState(materialData.type)
  const wrapperRef = React.useRef(null);
  function useOutsideAlerter(ref) {
    React.useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setOpenDropdown(false)
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }
  useOutsideAlerter(wrapperRef);
  React.useEffect(() => {
    if (options.length == 0) {
      options.push({
        id: -1,
        text: 'Tidak ada opsi',
        onClick: () => {}
      })
    }
  }, []);
	return (
    <div 
      className={['flex items-center', className].join(' ')}
      ref={wrapperRef}
    >
      {label &&
        <div className='mr-2 text-sm'>{label}</div>
      }
      <div className="relative inline-block text-left w-full">
        <button 
          type="button" 
          className='inline-flex justify-between items-center w-full rounded-3xl border border-black shadow-sm px-4 py-3 bg-white text-2xl text-gray-700 hover:bg-gray-50 focus:outline-none relative z-20' 
          id="menu-button" 
          aria-expanded="true" 
          aria-haspopup="true"
          onClick={() => setOpenDropdown(!openDropdown)}
          disabled={options.length == 0 || options[0].id === -1}
        >
          {currentOption ? currentOption.text && currentOption.text : ''}
          <svg className="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
          </svg>
        </button>
        {openDropdown &&
          <div 
            className='origin-top-right z-10 absolute -translate-y-6 pt-5 right-0 w-full rounded-b-3xl shadow-lg bg-white ring-1 ring-black ring-opacity-5 border border-black focus:outline-none' 
            role="menu" 
            aria-orientation="vertical" 
            aria-labelledby="menu-button" 
            tabindex="-1"
            onBlur={() => setOpenDropdown(false)}
          >
            <div className="py-1" role="none">
              {options.map((option) => {
                if (option.id != currentOption.id) {
                  return (
                    <div 
                      className="text-gray-700 block px-4 py-2 text-2xl hover:bg-gray-50 rounded-b-2xl cursor-pointer" 
                      role="menuitem" 
                      tabindex="-1" 
                      id="menu-item-0"
                      onClick={() => {
                        setCurrentOption(option)
                        option.onClick()
                        setOpenDropdown(false)
                      }}
                    >
                      {option ? option.text && option.text : null}
                    </div>
                  )
                }
                return null
              })}
            </div>
          </div>
        }
      </div>
    </div>
	)
}

export default ModalDropdown;