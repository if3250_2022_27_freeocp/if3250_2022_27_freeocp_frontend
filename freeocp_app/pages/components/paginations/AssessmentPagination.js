import React from 'react';
import styles from '../../../styles/Buttons.module.css';
import AssessmentPaginationItem from './AssessmentPaginationItem';
import { Provider, useSelector, useDispatch } from "react-redux";

const AssessmentPagination = ({ }) => {
  const questions = useSelector(state => state.questions)
	return (
    <>
      <p className='text-center text-2xl font-semibold mt-8'>Daftar Soal</p>
      {questions.length === 0 ?
        <p className='text-center blue-text'>Belum terdapat soal</p>
      :
        <div className='mt-8 grid gap-x-1 gap-y-1 grid-cols-5 px-12'>
          {questions.map((value, index) => (
            <AssessmentPaginationItem text={index + 1} />
          ))}
        </div>
      }
    </>
	)
}

export default AssessmentPagination;