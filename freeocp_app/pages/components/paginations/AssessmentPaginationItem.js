import styles from '../../../styles/Buttons.module.css';
import PrimaryButton from '../buttons/PrimaryButton';
import SecondaryButton from '../buttons/SecondaryButton';
import DangerButton from '../buttons/DangerButton';

const AssessmentPaginationItem = ({ text, className, onClick, onMouseHover }) => {
	return (
		<button 
			className={[styles.primaryButton, className, 'rounded-[10px] p-2'].join(' ')}
			onClick={() => {
        window.location.href = `#question_${Number(text) - 1}`;
        window.scrollBy(0,-90);
      }}
      onMouseHover={onMouseHover}
		>
				{text}
		</button>
	)
}

export default AssessmentPaginationItem;