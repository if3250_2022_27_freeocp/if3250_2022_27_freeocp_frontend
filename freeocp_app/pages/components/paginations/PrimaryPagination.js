import React from 'react';
import styles from '../../../styles/Buttons.module.css';
import Router, { useRouter } from 'next/router'

const PrimaryPagination = ({ text, className, onClick, onMouseHover, pages=20, onItemClicked }) => {
  const router = useRouter()
  const [offset, setOffset] = React.useState(0)
  React.useEffect(() => {
    let page = router.query.page
    console.log(page)
    if (page <= pages - 4) {
      setOffset(page - 1)
    }
    else {
      setOffset(pages - 4)
    }
    
    // onItemClicked(page)
  }, [router])
  const renderPages = () => {
    let result = []
    if (pages > 4) {
      for (let i=0+offset; i<3+offset; i++) {
        result.push(
          <a 
            className="bg-white cursor-pointer text-black relative inline-flex items-center px-3 py-2 text-md font-medium hover:bg-gray-50"
            onClick={() => onItemClicked(i + 1)}
          >
            {i + 1}
          </a>
        )
      }
      if (offset < pages - 4) {
        result.push(
          <span className="relative inline-flex items-center px-3 py-2 bg-white text-md font-medium text-gray-700"> ... </span>
        )
      }
      result.push(
        <span 
          className="bg-white cursor-pointer text-black relative inline-flex items-center px-3 py-2 text-md font-medium hover:bg-gray-50"
          onClick={() => {
            setOffset(pages - 4)
            onItemClicked(pages)
          }}
        > 
          {pages} 
        </span>
      )
    }
    else {
      for (let i=0; i<pages; i++) {
        result.push(
          <a 
            className="bg-white cursor-pointer text-black relative inline-flex items-center px-3 py-2 text-md font-medium hover:bg-gray-50"
            onClick={() => onItemClicked(i + 1)}
          >
            {i + 1}
          </a>
        )
      }
    }
    return result
  }
	return (
    <div>
      <nav className="relative z-0 inline-flex items-center rounded-md -space-x-px" aria-label="Pagination">
        <a 
          className="relative cursor-pointer inline-flex items-center px-2 py-2 rounded-l-md bg-white text-sm font-medium text-black hover:bg-gray-50"
          onClick={() => {
            if (offset >= 1) {
              setOffset(offset - 1)
            }
          }}
        >
          <span className="sr-only">Previous</span>
          <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
          </svg>
        </a>
        
        {renderPages()}

        <a 
          className="relative cursor-pointer inline-flex items-center px-2 py-2 rounded-r-md bg-white text-sm font-medium text-black hover:bg-gray-50"
          onClick={() => {
            if (offset <= pages - 5) {
              setOffset(offset + 1)
            }
          }}
        >
          <span className="sr-only">Next</span>
          <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
          </svg>
        </a>
      </nav>
    </div>
	)
}

export default PrimaryPagination;