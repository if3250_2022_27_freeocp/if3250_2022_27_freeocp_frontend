import React from 'react';
import styles from '../../../styles/Assessments.module.css';
import SectionContainer from '../containers/SectionContainer';
import { useSelector, useDispatch } from "react-redux";
import { addMultipleAnswerSolution, deleteMultipleAnswerSolution, changeQuestionOption, deleteQuestionOption } from '../../../redux/actions/DoAssessmentAction';

const CheckboxButtonOptionView = ({ optionIdx=null, name=null }) => {
  if (optionIdx == null || name == null) {
    return null
  }
	const dispatch = useDispatch()
  const options = useSelector(state => state.questions[name].options)
  const solutions = useSelector(state => state.questions[name].solutions)
  const isCurate = useSelector(state => state.isCurate)
  const isOptionChecked = () => {
    let found = false
    let solutionIndex = -1
    for (let i=0;i<solutions.length;i++) {
      if (solutions[i].index === optionIdx) {
        found = true
        solutionIndex = i
        break
      }
    }
    return {
      found,
      solutionIndex
    }
  }
  const handleCheckOption = () => {
    let { found, solutionIndex } = isOptionChecked()
    if (found) {
      dispatch(deleteMultipleAnswerSolution(name, solutionIndex))
    } else {
      dispatch(addMultipleAnswerSolution(name, optionIdx, options[optionIdx]))
    }
  }
  return (
    <div className='flex my-3'>
      <div 
        className={['flex flex-row rounded-3xl w-full grow border-gray-800 items-center',
        (options[optionIdx].text === '' || !options[optionIdx].text) && 'h-12', 
        isCurate && solutions.find((solution) => {
          return solution.index === optionIdx
        }) && styles.curateSolution].join(' ')}
        style={{ borderWidth: '1px' }}
      >
        <input 
          type="checkbox" 
          checked={solutions.find((solution) => {
            return solution.index === optionIdx
          })}
          onChange={() => handleCheckOption()}
          name={name}
          className='ml-3 mr-2 cursor-pointer form-check-input'
          disabled={isCurate}
        ></input>
        <div className={[styles.optionAlphabet, 'w-10 mr-2 flex items-center justify-center'].join(' ')}>
          {String.fromCharCode(optionIdx + 65)}.
        </div>
        <p 
          className="py-2 text-lg w-full mr-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
        >
          {options[optionIdx].text && options[optionIdx].text !== '' ? options[optionIdx].text : options[optionIdx]}
        </p>
      </div>
    </div>
	)
}

export default CheckboxButtonOptionView;
