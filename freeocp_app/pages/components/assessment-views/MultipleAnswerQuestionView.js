import React from 'react';
import styles from '../../../styles/Buttons.module.css';
import SectionContainer from '../containers/SectionContainer';
import CheckboxButtonOptionView from './CheckboxButtonOptionView';
import DashedButton from '../buttons/DashedButton';
import { useSelector } from "react-redux";

const MultipleAnswerQuestionView = ({ questionIdx }) => {
	if (questionIdx == null) {
    return null
  }
	const description = useSelector(state => state.questions[questionIdx].description)
	const options = useSelector(state => state.questions[questionIdx].options)
	return (
		<div className='my-6 flex items-start'>
			<SectionContainer title={`${questionIdx + 1} (Pilihan Berganda)`}>
				<div className='py-6 px-12'>
					<div className='flex flex-row rounded-3xl w-full grow py-1'>
						<p className='text-xl mb-2'>{description}</p>
					</div>
					{options.map((optionData, idx) => (
						<CheckboxButtonOptionView
							optionIdx={idx}
							name={questionIdx}
						/>
					))}
				</div>
			</SectionContainer>
		</div>
	)
}

export default MultipleAnswerQuestionView;