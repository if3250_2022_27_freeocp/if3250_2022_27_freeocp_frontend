import React from 'react';
import styles from '../../../styles/Assessments.module.css';
import SectionContainer from '../containers/SectionContainer';
import { useSelector, useDispatch } from "react-redux";
import { changeMultipleChoiceSolution, changeQuestionOption, deleteQuestionOption } from '../../../redux/actions/DoAssessmentAction';

const RadioButtonOptionView = ({ optionIdx, name }) => {
  if (optionIdx == null || name == null) {
    return null
  }
  const dispatch = useDispatch()
  const options = useSelector(state => state.questions[name].options)
  const solution = useSelector(state => state.questions[name].solution)
  const isCurate = useSelector(state => state.isCurate)
  console.log(options[optionIdx].text)
	return (
    <div className='flex my-3'>
      <div 
        className={['flex flex-row rounded-3xl w-full grow border-gray-800 items-center', 
        isCurate && solution.index === optionIdx && styles.curateSolution, 
        (options[optionIdx].text === '' || !options[optionIdx].text) && 'h-12'].join(' ')}
        style={{ borderWidth: '1px' }}
      >
        <input 
          type="radio" 
          checked={solution.index === optionIdx}
          onChange={() => dispatch(changeMultipleChoiceSolution(name, optionIdx, options[optionIdx]))}
          name={name}
          className='ml-3 mr-2 cursor-pointer'
          disabled={isCurate}
        ></input>
        <div className={[styles.optionAlphabet, 'w-10 mr-2 flex items-center justify-center'].join(' ')}>
          {String.fromCharCode(optionIdx + 65)}.
        </div>
        <p 
          className="py-2 text-lg w-full mr-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
        >
          {options[optionIdx].text && options[optionIdx].text !== '' ? options[optionIdx].text : options[optionIdx]}
        </p>
      </div>
    </div>
	)
}

export default RadioButtonOptionView;
