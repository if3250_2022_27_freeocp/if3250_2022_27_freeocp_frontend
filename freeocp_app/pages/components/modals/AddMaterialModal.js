// import styles from '../../../styles/Buttons.module.css';
import React from "react";
import PrimaryButton from "../buttons/PrimaryButton";
import SecondaryButton from "../buttons/SecondaryButton";
import styles from '../../../styles/Modals.module.css'
import ModalDropdown from "../dropdowns/ModalDropdown";
import { useSelector, useDispatch } from "react-redux";
import { addChapterMaterial } from '../../../redux/actions/CreateCourseAction';

const AddMaterialModal = ({ visible, setVisible, title, chapterIdx }) => {
  if (visible == null || setVisible == null || title == null || chapterIdx == null) {
    return null
  }
  const materialList = useSelector(state => state.materialList)
  const dispatch = useDispatch()
  const initMaterialData = {
    title: '',
    type: {
      id: 0,
      text: 'Artikel',
      onClick: () => {handleChangeType(materialTypeOptions[0])},
      type: 'article'
    },
    link: '',
    id: ''
  }
  const [materialData, setMaterialData] = React.useState(initMaterialData)
  const handleChangeType = (typeValue) => {
    let temp = materialData
    materialData['type'] = typeValue
    setMaterialData({...temp})
    console.log(materialData)
  }
  const materialTypeOptions = [
    {
      id: 0,
      text: 'Artikel',
      onClick: () => {handleChangeType(materialTypeOptions[0])},
      type: 'article'
    },
    {
      id: 1,
      text: 'Video',
      onClick: () => {handleChangeType(materialTypeOptions[1])},
      type: 'video'
    },
    // {
    //   id: 2,
    //   text: 'Latihan',
    //   onClick: () => {handleChangeType(materialTypeOptions[2])},
    //   type: 'assessment'
    // },
  ]
  // React.useEffect(() => {
  //   setValue(initialValue)
  // }, [initialValue])
  const handleOnInputListener = () => {
    if (textArea) {
      const grower = document.querySelector('#growWrap')
      const textarea = document.querySelector("#growArea");
      grower.dataset.replicatedValue = textarea.value;
    }
  }
  const handleUpdateValue = () => {
    let temp = materialData
    temp['type'] = temp['type']['type']
    dispatch(addChapterMaterial(chapterIdx, temp))
  }
  const handleChangeMaterialData = (value, attr) => {
    let temp = materialData
    temp[attr] = value
    setMaterialData({...temp})
  }
  if (visible) {
    return (
      <div className="fixed z-30 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <div className="fixed inset-0 bg-gray-700 bg-opacity-80 transition-opacity" aria-hidden="true"></div>
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
          <div className="relative inline-block align-bottom bg-white rounded-3xl text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full">
            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4" >
              <div className="sm:flex justify-between items-center mb-2">
                <div className='w-14' />
                <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                  <h3 className="text-2.5xl font-semibold" id="modal-title">{title}</h3>
                </div>
                <img 
                  src='modals/cancel_icon.png'
                  className='w-14 cursor-pointer'
                  onClick={() => {
                    setMaterialData(initMaterialData)
                    setVisible(false)
                  }}
                 />
              </div>
              {/* <div class="pb-1">
                <div class="border-t border-black"></div>
              </div> */}
              <div className='my-3'>
                <p>Judul</p>
                <div className='rounded-3xl border-black' style={{ borderWidth: '1px' }}>
                  <div className='flex flex-row rounded-2xl px-1 py-1 my-1 border-gray-800'>
                    <input
                      className="appearance-none w-full ml-2 mt-1 mb-1 text-2xl text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                      value={materialData.title}
                      onChange={(e) => handleChangeMaterialData(e.target.value, 'title')}
                    ></input>
                  </div>
                </div>
              </div>
              <div className='my-3'>
                <p>Jenis</p>
                <ModalDropdown
                  options={materialTypeOptions}
                  className='w-full'
                  width='full'
                  materialData={materialData}
                />
              </div>
              <div className='my-3'>
                <p>Link</p>
                <div className='rounded-3xl border-black' style={{ borderWidth: '1px' }}>
                  <div className='flex flex-row rounded-2xl px-1 py-1 my-1 border-gray-800'>
                    <input
                      className="appearance-none w-full ml-2 mt-1 mb-1 text-2xl text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                      value={materialData.link}
                      onChange={(e) => handleChangeMaterialData(e.target.value, 'link')}
                    ></input>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-gray-50 px-4 py-3 pb-5 flex justify-center">
              <SecondaryButton
                text='Batal'
                className='w-32 mr-1'
                onClick={() => {
                  setMaterialData(initMaterialData)
                  setVisible(false)
                }}
              />
              <PrimaryButton
                text='Tambah'
                className='w-32 ml-1 flex justify-center'
                onClick={() => {
                  handleUpdateValue()
                  setMaterialData(initMaterialData)
                  setVisible(false)
                }}
              />
            </div>
          </div>
        </div>
      </div>
    )
  } else return null
}

export default AddMaterialModal;