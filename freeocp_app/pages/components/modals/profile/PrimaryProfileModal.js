// import styles from '../../../styles/Buttons.module.css';
import React from "react";
import PrimaryButton from "../../buttons/PrimaryButton";
import SecondaryButton from "../../buttons/SecondaryButton";
import styles from '../../../../styles/Modals.module.css'
import { updateProfile } from "../../../api/profile";

const PrimaryProfileModal = ({ visible, setVisible, title, initialValue, setFinalValue, textArea=false, type='' }) => {
  const [value, setValue] = React.useState('')
  React.useEffect(() => {
    setValue(initialValue)
  }, [initialValue])
  const handleOnInputListener = () => {
    if (textArea) {
      const grower = document.querySelector('#growWrap')
      const textarea = document.querySelector("#growArea");
      grower.dataset.replicatedValue = textarea.value;
    }
  }
  const handleUpdateValue = () => {
    let params = {}
    params[type] = value
    console.log(params)
    updateProfile(params).then(function(res) {
      setValue(value)
      setFinalValue(value)
      setVisible(false)
    }).catch(function(err) {
      console.log(err)
    })
  }
  if (visible) {
    return (
      <div className="fixed z-30 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <div className="fixed inset-0 bg-gray-700 bg-opacity-80 transition-opacity" aria-hidden="true"></div>
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
          <div className="relative inline-block align-bottom bg-white rounded-3xl text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full">
            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4" >
              <div className="sm:flex justify-between items-center mb-2">
                <div className='w-14' />
                <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                  <h3 className="text-2.5xl font-semibold" id="modal-title">{title}</h3>
                </div>
                <img 
                  src='modals/cancel_icon.png'
                  className='w-14 cursor-pointer'
                  onClick={() => {
                    setValue(initialValue)
                    setVisible(false)
                  }}
                 />
              </div>
              <div className='rounded-3xl border-black' style={{ borderWidth: '1px' }}>
                {textArea ?
                  <div className={['border-0 flex flex-row rounded-3xl px-1 py-1', styles.growWrap].join(' ')} id='growWrap'>
                    <textarea 
                      className={['focus:outline-none rounded-3xl', styles.growArea].join(' ')} 
                      value={value}
                      onChange={(e) => setValue(e.target.value)}
                      onInput={() => handleOnInputListener()}
                      id='growArea'
                    ></textarea>
                  </div>
                :
                  <div className='flex flex-row rounded-2xl px-1 py-1 my-1 border-gray-800'>
                    <input
                      className="appearance-none w-full ml-2 mt-1 mb-1 text-2xl text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                      value={value}
                      onChange={(e) => setValue(e.target.value)}
                    ></input>
                  </div>
                }
              </div>
            </div>
            <div className="bg-gray-50 px-4 py-3 pb-5 flex justify-center">
              <SecondaryButton
                text='Batal'
                className='w-32 mr-1'
                onClick={() => {
                  setValue(initialValue)
                  setVisible(false)
                }}
              />
              <PrimaryButton
                text='Ubah'
                className='w-32 ml-1'
                onClick={() => handleUpdateValue()}
              />
            </div>
          </div>
        </div>
      </div>
    )
  } else return null
}

export default PrimaryProfileModal;