// import styles from '../../../styles/Buttons.module.css';
import React from "react";
import PrimaryButton from "../buttons/PrimaryButton";
import SecondaryButton from "../buttons/SecondaryButton";
import styles from '../../../styles/Modals.module.css'
import ModalDropdown from "../dropdowns/ModalDropdown";
import { useSelector, useDispatch } from "react-redux";
import { addChapterMaterial } from '../../../redux/actions/CreateCourseAction';
import Router, { useRouter } from 'next/router'

const SuccessModal = ({ visible, setVisible, title, route='/', query={} }) => {
  if (visible) {
    return (
      <div className="fixed z-30 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <div className="fixed inset-0 bg-gray-700 bg-opacity-80 transition-opacity" aria-hidden="true"></div>
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
          <div className="relative inline-block align-bottom bg-white rounded-3xl text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full">
            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4" >
              <div className="sm:flex justify-center items-center mb-2">
                <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                  <h3 className="text-2.5xl font-semibold" id="modal-title">{title}</h3>
                </div>
              </div>
            </div>
            <div className="bg-gray-50 px-4 py-3 pb-5 flex justify-center">
              <PrimaryButton
                text='Lanjutkan'
                className='w-32 ml-1 flex justify-center'
                onClick={() => {
                  Router.push({
                    pathname: route,
                    query: query
                  })
                }}
              />
            </div>
          </div>
        </div>
      </div>
    )
  } else return null
}

export default SuccessModal;