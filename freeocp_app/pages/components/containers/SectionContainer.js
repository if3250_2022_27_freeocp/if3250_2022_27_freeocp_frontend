import React from 'react';
import styles from '../../../styles/Containers.module.css';

const SectionContainer = ({ title="Informasi", children }) => {
  const [openSection, setOpenSection] = React.useState(true)
	return (
		<div className='w-full'>
      {openSection ?
        <div 
          className={['rounded-t-3xl px-5 py-1.5 text-xl font-medium cursor-pointer flex justify-between', styles.header].join(' ')}
          onClick={() => setOpenSection(!openSection)}
        >
          {title}
          <img src='assessments/icon_chevron_up.svg' className='w-7' />
        </div>
      :
        <div 
          className={['rounded-3xl px-5 py-1.5 text-xl font-medium cursor-pointer flex justify-between', styles.header].join(' ')}
          onClick={() => setOpenSection(!openSection)}
        >
          {title}
          <img src='assessments/icon_chevron_down.svg' className='w-7' />
        </div>
      }
      {openSection &&
        <div className={['rounded-b-3xl', styles.content].join(' ')}>
          {children}
        </div>
      }
		</div>
	)
}

export default SectionContainer;