import React from 'react';
import AccordionItem from './AccordionItem';
import styles from '../../../styles/Accordions.module.css'

const AccordionList = ({ materialData=[], isEdit=false }) => {
	return (
    <>
      {materialData.map((material) => (
        <AccordionItem material={material} isEdit={isEdit} />
      ))}
    </>
	)
}

export default AccordionList;
