import React from 'react';
import styles from '../../../styles/Accordions.module.css'
import { useSelector, useDispatch } from "react-redux";
import SecondaryButton from '../buttons/SecondaryButton';
import Router from 'next/router'

const AccordionItem = ({ material=null, isEdit=false }) => {
  const [open, setOpen] = React.useState(false)
  const [contentHeight, setContentHeight] = React.useState('0px')

  const content = React.useRef(null);
  const courseId = useSelector(state => state.courseId)

  const handleChangeContent = () => {
    setContentHeight(open ? "0px" : `${content.current.scrollHeight - 24}px`);
    setOpen(!open)
  }
  if (material != null) {
    return (
      <div className='my-2'>
        <div className='relative z-10'>
          <button 
            type="button" 
            className="flex justify-between items-center px-8 py-3 w-full font-medium text-left text-gray-900 bg-gray-100 rounded-3xl border border-black hover:bg-gray-100 dark:hover:bg-gray-800"
            style={{ background: 'rgba(211, 240, 244, 0.9)' }}
            onClick={() => handleChangeContent()}
          >
            <span className='text-l font-bold'>{material.chapter_title}</span>
            <svg 
              data-accordion-icon
              className={['w-6 h-6 shrink-0', open && 'rotate-180'].join(' ')}
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
            </svg>
          </button>
        </div>
        <div
          ref={content}
          style={{maxHeight: `${contentHeight}`, overflow: open === false && 'hidden' }}
          className={['duration-150 -translate-y-8 relative'].join(' ')}
        >
          <div 
            className={['p-6 pt-5 pb-3 border rounded-b-3xl border-black border-t-0 relative pt-11 text-l'].join(' ')}
          >
            {material.content.map((item) => (
              <>
                {item.type === 'assessment' && (item.status === 'accepted' || isEdit) &&
                  <div className={['flex items-center', styles.itemColor].join('')}>
                    <img src={`accordions/icon_${item.type}.svg`} className={[styles.icon, 'mr-2 my-1'].join(' ')} />
                    {item.score != null && !isEdit ?
                      <a className={['mr-4 my-1', styles.itemColor].join(' ')}>{item.title}</a>
                    :
                      <a
                        // href={`/view-assessment?course_id=${courseId}&assessment_id=${item.link}`}
                        target='_blank'
                        className={['cursor-pointer mr-4 my-1', styles.itemColor].join(' ')}
                        onClick ={() => {
                          Router.push({
                            pathname: `/view-assessment`,
                            query: { courseId: courseId, assessment_id: item.link },
                          })
                        }}
                      >
                        {item.title}
                      </a>
                    }
                    {isEdit ?
                      <>
                        {
                          item.status === 'in review' ?
                            <div
                              className={[
                                styles.review,
                                'rounded-xl text-xs font-bold px-4 py-0.5 my-1'
                              ].join(' ')}
                            >
                              Diproses
                            </div>
                          : item.status === 'accepted' ?
                            <div
                              className={[
                                styles.accepted,
                                'rounded-xl text-xs font-bold px-4 py-0.5 my-1'
                              ].join(' ')}
                            >
                              Diterima
                            </div>
                          : item.status === 'pending' ?
                            <div
                              className={[
                                styles.pending,
                                'rounded-xl text-xs font-bold px-4 py-0.5 my-1'
                              ].join(' ')}
                            >
                              Diantrikan
                            </div>
                          :
                            <div
                              className={[
                                styles.rejected,
                                'rounded-xl text-xs font-bold px-4 py-0.5 my-1'
                              ].join(' ')}
                            >
                              Ditolak
                            </div>
                        }
                      </>
                    :
                      <>
                        {item.score != null &&
                          <a
                            // href={`/view-assessment-score?assessment_id=${item.link}`}
                            target='_blank'
                            onClick ={() => {
                              Router.push({
                                pathname: `/view-assessment-score`,
                                query: { courseId: courseId, assessment_id: item.link },
                              })
                            }}
                          >
                            <SecondaryButton
                              text='Cek Nilai'
                              className='text-xs font-bold px-4 py-0.5 my-1'
                            />
                          </a>
                        }
                      </>
                    }
                  </div>
                }
                {item.type != 'assessment' &&
                  <div className={['flex items-center', styles.itemColor].join('')}>
                    <img src={`accordions/icon_${item.type}.svg`} className={[styles.icon, 'mr-2 my-1'].join(' ')} />
                    <a
                      href={item.link}
                      target='_blank'
                      className={['cursor-pointer my-1', styles.itemColor].join(' ')}
                    >
                      {item.title}:&nbsp;{item.link}
                    </a>
                  </div>
                }
              </>
            ))}
          </div>
        </div>
      </div>
    )
  }
  else return null
}

export default AccordionItem;
