import React from 'react';
import styles from '../../../styles/Accordions.module.css'
import DashedButton from '../buttons/DashedButton';
import AddIconButton from '../buttons/AddIconButton';
import AddMaterialModal from '../modals/AddMaterialModal';
import { useSelector, useDispatch } from "react-redux";
import { deleteChapter, changeChapterTitle, deleteChapterMaterial } from '../../../redux/actions/CreateCourseAction';
import Router, { useRouter } from 'next/router'

const AccordionItemCreate = ({ material=null, chapterIdx=null }) => {
  if (material == null || chapterIdx == null) {
    return null
  }
  const materialList = useSelector(state => state.materialList)
  const courseId = useSelector(state => state.courseId)
  const allowAssessment = useSelector(state => state.allowAssessment)
  const dispatch = useDispatch()

  const [open, setOpen] = React.useState(false)
  const [contentHeight, setContentHeight] = React.useState('0px')

  const [addMaterialModalVisible, setAddMaterialModalVisible] = React.useState(false)
  const [newMaterial, setNewMaterial] = React.useState([])

  const content = React.useRef(null);

  const handleChangeContent = () => {
    setContentHeight(open ? "0px" : `${content.current.scrollHeight - 24}px`);
    setOpen(!open)
  }
  React.useEffect(() => {
    setContentHeight(open ? `${content.current.scrollHeight - 24}px` : contentHeight);
  }, [materialList])
  if (material != null) {
    return (
      <div className='my-2 flex w-full items-start'>
        <div className='w-full mr-2'>
          <AddMaterialModal
            visible={addMaterialModalVisible} 
            setVisible={setAddMaterialModalVisible}
            title='Tambah Materi'
            initialValue={newMaterial}
            setFinalValue={setNewMaterial}
            chapterIdx={chapterIdx}
          />
          <div className='relative z-10'>
            <button 
              type="button" 
              className="flex cursor-auto justify-between items-center px-8 py-3 w-full font-medium text-left text-gray-900 bg-gray-100 rounded-3xl border border-black hover:bg-gray-100 dark:hover:bg-gray-800"
              style={{ background: 'rgba(211, 240, 244, 0.9)' }}
            >
              {/* <span className='text-l font-bold'>{material.chapter_title}</span> */}
              <input 
                className="appearance-none py-1 accordionInput text-l w-full mx-2 text-black leading-tight focus:outline-none focus:shadow-outline" 
                type="text" 
                placeholder="Ketikkan Judul Bagian di sini"
                style={{ background: 'rgba(255, 255, 255, 0.9)' }}
                value={materialList[chapterIdx].chapter_title}
                onChange={(e) => dispatch(changeChapterTitle(chapterIdx, e.target.value))}
              ></input>
              <svg 
                data-accordion-icon
                className={['w-6 h-6 shrink-0 cursor-pointer', open && 'rotate-180'].join(' ')}
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
                onClick={() => handleChangeContent()}
              >
                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
              </svg>
            </button>
          </div>
          <div
            ref={content}
            style={{maxHeight: `${contentHeight}`, overflow: open === false && 'hidden' }}
            className={['duration-150 -translate-y-8 relative'].join(' ')}
          >
            <div 
              className={['p-6 pt-5 pb-3 border rounded-b-3xl border-black border-t-0 relative pt-11 text-l'].join(' ')}
            >
              {material.content.map((item, chapterMaterialIdx) => (
                <div className='flex items-center justify-between'>
                  <div className={['flex items-center', styles.itemColor].join('')}>
                    <img src={`accordions/icon_${item.type}.svg`} className={[styles.icon, 'mr-2 my-1'].join(' ')} />
                    {item.type === 'assessment' ?
                      <a href={`/edit-assessment?course_id=${courseId}&assessment_id=${item.link}`} target='_blank' className={['cursor-pointer my-1', styles.itemColor].join(' ')}>{item.title}</a>
                    :
                      <a href={item.link} target='_blank' className={['cursor-pointer my-1', styles.itemColor].join(' ')}>{item.title}:&nbsp;{item.link}</a>
                    }
                  </div>
                  <img
                    src='assessments/icon_remove.svg'
                    className='cursor-pointer'
                    onClick={() => dispatch(deleteChapterMaterial(chapterIdx, chapterMaterialIdx))}
                  />
                </div>
              ))}
              {/* <DashedButton 
                text='Tambah Materi Untuk Bagian Ini' 
                className='w-full py-2 mt-1'
                onClick={() => setAddMaterialModalVisible(true)} 
              /> */}
              {material.content.length > 0 &&
                <div class="py-1.5">
                  <div class="border-t border-black"></div>
                </div>
              }
              <AddIconButton
                text='Tambah Materi' 
                className='w-full'
                onClick={() => setAddMaterialModalVisible(true)} 
              />
              {allowAssessment && courseId && courseId != '' &&
                <AddIconButton
                  text='Tambah Latihan' 
                  className='w-full'
                  onClick={() => {
                    Router.push({
                      pathname: '/create-assessment',
                      query: {
                        courseId: courseId,
                        chapterId: materialList[chapterIdx].chapter_id
                      }
                    })
                  }} 
                />
              }
            </div>
          </div>
        </div>
        <img
          src='assessments/icon_remove.svg'
          className='cursor-pointer'
          onClick={() => dispatch(deleteChapter(chapterIdx))}
        />
      </div>
    )
  }
  else return null
}

export default AccordionItemCreate;
