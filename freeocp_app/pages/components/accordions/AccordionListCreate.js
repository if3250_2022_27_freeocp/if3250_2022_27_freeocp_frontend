import React from 'react';
import AccordionItemCreate from './AccordionItemCreate';
import styles from '../../../styles/Accordions.module.css';
import DashedButton from '../buttons/DashedButton';
import { useSelector, useDispatch } from "react-redux";
import { addChapter } from '../../../redux/actions/CreateCourseAction';

const AccordionListCreate = ({ course=null }) => {
  if (course == null) {
    return null
  }
  const materialList = useSelector(state => state.materialList)
  const dispatch = useDispatch()
	return (
    <>
      {materialList.map((material, chapterIdx) => (
        <AccordionItemCreate material={material} chapterIdx={chapterIdx} />
      ))}
      <DashedButton
        text='Tambah Bagian'
        className='w-full mt-2'
        onClick={() => dispatch(addChapter())} 
      />
    </>
	)
}

export default AccordionListCreate;
