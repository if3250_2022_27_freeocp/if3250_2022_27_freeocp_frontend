import React from 'react';
import AccordionItemCreate from './AccordionItemCreate';
import styles from '../../../styles/Accordions.module.css';
import DashedButton from '../buttons/DashedButton';
import { useSelector, useDispatch } from "react-redux";
import { changeChapter, addChapter } from '../../../redux/actions/CreateCourseAction';

const AccordionListEdit = ({ materialData=[] }) => {
  const materialList = useSelector(state => state.materialList)
  const dispatch = useDispatch()

  React.useEffect(() => {
    dispatch(changeChapter(materialData))
  }, [materialList])
	return (
    <>
      {materialList.map((material, chapterIdx) => (
        <AccordionItemCreate material={material} chapterIdx={chapterIdx} />
      ))}
      <DashedButton
        text='Tambah Bagian'
        className='w-full mt-2'
        onClick={() => dispatch(addChapter())}
      />
    </>
	)
}

export default AccordionListEdit;
