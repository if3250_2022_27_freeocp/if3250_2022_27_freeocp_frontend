import React from 'react';
import styles from '../../../styles/Navigations.module.css';
import PrimaryButton from '../buttons/PrimaryButton';
import SecondaryButton from '../buttons/SecondaryButton';
import BlackOutlinedButton from '../buttons/BlackOutlinedButton';
import Router, { useRouter } from 'next/router'
import { isUserAuth, logout } from '../../api/auth';

const Footer = () => {
	const router = useRouter()
	return (
		<div
      className={['flex justify-between py-6 px-6 items-center', styles.navbarShadow].join(' ')}
      style={{ background: '#D8E3E7' }}
    >
			<div className='flex flex-col'>
				<img 
					src='logo.png'
					className={['mr-9 cursor-pointer', styles.logo].join(' ')}
					onClick={() => window.location.href = '/'}
				/>
				<div class="pt-4 pb-4">
          <div class="border-t border-black" style={{ width: '500px' }}></div>
        </div>
        <p>
          ©2022 ROKETIN – PT Roketin Kreatif Teknologi. ALL RIGHTS RESERVED
        </p>
			</div>
			<div className='flex'>
				<div className={styles.mediaIcon}>
					<img src='footer/Facebook.svg' />
				</div>
				<div className={styles.mediaIcon}>
					<img src='footer/Instagram.svg' />
				</div>
				<div className={styles.mediaIcon}>
					<img src='footer/Twitter.svg' />
				</div>
				<div className={styles.mediaIcon}>
					<img src='footer/Youtube.svg' />
				</div>
			</div>
		</div>
	)
}

export default Footer;
