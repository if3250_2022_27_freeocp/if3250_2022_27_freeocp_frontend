import React from 'react';
import styles from '../../../styles/Navigations.module.css';
import PrimaryButton from '../buttons/PrimaryButton';
import SecondaryButton from '../buttons/SecondaryButton';
import BlackOutlinedButton from '../buttons/BlackOutlinedButton';
import Router, { useRouter } from 'next/router'
import { isUserAuth, logout } from '../../api/auth';

const Navbar = () => {
	const router = useRouter()
	const [isAuth, setIsAuth] = React.useState(false);
	const [isCurator, setIsCurator] = React.useState(false);
	const [isAdmin, setIsAdmin] = React.useState(false);
	const [openCuratorDropdown, setOpenCuratorDropdown] = React.useState(false);
	const [openProfileDropdown, setOpenProfileDropdown] = React.useState(false);
	const [searchQuery, setSearchQuery] = React.useState('')
	const handleLogout = () => {
		logout().then(function(res) {
      localStorage.removeItem('token')
      localStorage.removeItem('email')
			setIsAuth(false)
			setOpenProfileDropdown(false)
      window.location.href = '/'
    }).catch(function(err) {
      console.log(err)
    })
	}
	const handleSearch = () => {
		Router.push({
			pathname: '/search',
			query: { query: searchQuery, page: 1 },
		})
	}
	React.useEffect(() => {
		isUserAuth().then(function(res) {
			console.log(res)
      setIsAuth(true)
			setIsAdmin(res.data.is_admin ? res.data.is_admin : false)
			setIsCurator(res.data.is_curator)
    }).catch(function(err) {
      localStorage.removeItem('token')
      localStorage.removeItem('email')
			setIsAuth(false)
			setOpenProfileDropdown(false)
    })
		if (router.query.query) {
			setSearchQuery(router.query.query)
		}
	}, [])
	if (isAuth) {
		return (
			<div className={['flex justify-between py-3 px-6 items-center fixed left-0 right-0 z-50', styles.navbarShadow].join(' ')}>
				<div className='flex flex-row items-center grow'>
					<img 
						src='logo.png'
						className={['mr-9 cursor-pointer', styles.logo].join(' ')}
						onClick={() => window.location.href = '/'}
					/>
					<button type="button" style={{ fontSize: '22px' }} className="inline-flex items-center mr-9 rounded-md py-2 bg-white text-sm font-medium hover:bg-gray-50 focus:outline-none" id="menu-button" aria-expanded="true" aria-haspopup="true">
						Kategori
						<svg className="-mr-1 ml-2 h-7 w-7" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
							<path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
						</svg>
					</button>
					{/* <div class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
						<div class="py-1" role="none">
							<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-0">Account settings</a>
							<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-1">Support</a>
							<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-2">License</a>
							<form method="POST" action="#" role="none">
								<button type="submit" class="text-gray-700 block w-full text-left px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-3">Sign out</button>
							</form>
						</div>
					</div> */}
					<div className='flex flex-row rounded-3xl grow px-3 py-1 mr-24 border-gray-800' style={{ borderWidth: '1px' }}>
						{/* <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="w-6 h-6" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
							<path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
						</svg> */}
						<img 
							src='search_icon.png'
							className='w-8 h-8 hover:bg-gray-50 cursor-pointer rounded-full'
							onClick={() => handleSearch()} 
						/>
						<input 
							className="appearance-none w-full ml-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
							type="text" 
							placeholder="Cari kursus"
							value={searchQuery}
							onChange={(e) => setSearchQuery(e.target.value)}
						></input>
					</div>
				</div>
				<div className='flex'>
					{isCurator &&
						<div 
							onMouseOver={() => setOpenCuratorDropdown(true)}
							onMouseLeave={() => setOpenCuratorDropdown(false)} 
							className='relative'
						>
							<BlackOutlinedButton 
								text='Pilih Soal' 
								className='mr-2.5'
							/>
							{openCuratorDropdown &&
								<div className='absolute -translate-x-4'>
									<div className='h-4'></div>
									<div
										className="px-2 py-3 rounded-b-3xl shadow-xl flex flex-col items-center -z-50"
										style={{ background: 'white' }}
									>
										<BlackOutlinedButton 
											className='whitespace-nowrap' 
											text='Daftar Latihan' 
											style={{ padding: '0.625em 1.1em' }}
											blueHover
											onClick={() => {
												setOpenCuratorDropdown(false);
												window.location.href = '/curation?page=1'
											}}
										/>
										<BlackOutlinedButton 
											className='whitespace-nowrap mt-2' 
											text='Latihan Dipilih' 
											style={{ padding: '0.625em 1.1em' }}
											blueHover
											onClick={() => {
												setOpenCuratorDropdown(false);
												window.location.href = '/curation-chosen?page=1'
											}}
										/>
									</div>
								</div>
							}
						</div>
					}
					<BlackOutlinedButton 
						text='Kontribusi' 
						className='mr-2.5'
						blueHover
						onClick={() => {
							Router.push({
								pathname: '/create-course',
							})
						}}
					/>
					<div 
						onMouseOver={() => setOpenProfileDropdown(true)}
						onMouseLeave={() => setOpenProfileDropdown(false)} 
						className='relative'
					>
						<BlackOutlinedButton text='Profil' />
						{openProfileDropdown &&
							<div className='absolute right-0 translate-x-3.5'>
								<div className='h-4'></div>
								<div
									className="px-2 py-3 rounded-b-3xl shadow-xl flex flex-col items-center -z-50"
									style={{ background: 'white' }}
								>
									<p className='line-clamp-1 mb-2 text-sm blue-text' style={{ width: '105px' }}>
										{localStorage.getItem('email')}
									</p>
									<BlackOutlinedButton 
										className='whitespace-nowrap' 
										text='Lihat Profil' 
										style={{ padding: '0.625em 1.1em' }}
										blueHover
										onClick={() => {
											setOpenProfileDropdown(false);
											window.location.href = '/profile'
										}}
									/>
									{isAdmin &&
										<BlackOutlinedButton 
											className='mt-2' 
											text='Admin'
											blueHover
											onClick={() => {
												window.location.href = '/admin-dashboard?page=1'
											}}
										/>
									}
									<BlackOutlinedButton 
										className='mt-2' 
										text='Keluar'
										blueHover
										onClick={() => {
											handleLogout()
										}}
									/>
								</div>
							</div>
						}
					</div>
				</div>
			</div>
		)
	}
	return (
		<div className={['flex justify-between py-3 px-6 items-center fixed left-0 right-0 z-50', styles.navbarShadow].join(' ')}>
			<div className='flex flex-row items-center grow'>
				<img 
					src='logo.png'
					className={['mr-9 cursor-pointer', styles.logo].join(' ')}
					onClick={() => window.location.href = '/'}
				/>
				<button type="button" style={{ fontSize: '22px' }} className="inline-flex items-center mr-9 rounded-md py-2 bg-white text-sm font-medium hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500" id="menu-button" aria-expanded="true" aria-haspopup="true">
					Kategori
					<svg className="-mr-1 ml-2 h-7 w-7" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
						<path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
					</svg>
				</button>
				{/* <div class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
					<div class="py-1" role="none">
						<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-0">Account settings</a>
						<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-1">Support</a>
						<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-2">License</a>
						<form method="POST" action="#" role="none">
							<button type="submit" class="text-gray-700 block w-full text-left px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-3">Sign out</button>
						</form>
					</div>
				</div> */}
				<div className='flex flex-row rounded-3xl grow px-3 py-1 mr-48 border-gray-800' style={{ borderWidth: '1px' }}>
					{/* <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="w-6 h-6" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
						<path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
					</svg> */}
					<img 
						src='search_icon.png'
						className='w-8 h-8 hover:bg-gray-50 cursor-pointer rounded-full'
						onClick={() => handleSearch()} 
					/>
					<input 
						className="appearance-none w-full ml-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
						type="text" 
						placeholder="Cari kursus"
						value={searchQuery}
						onChange={(e) => setSearchQuery(e.target.value)}
					></input>
				</div>
			</div>
			<div>
				<SecondaryButton 
					text='Masuk' 
					className='mr-2.5' 
					onClick={() => {
						setOpenProfileDropdown(false);
						window.location.href = '/login'
					}} />
				<PrimaryButton 
					text='Daftar'
					onClick={() => {
						setOpenProfileDropdown(false);
						window.location.href = '/register'
					}}
				/>
			</div>
		</div>
	)
}

export default Navbar;
