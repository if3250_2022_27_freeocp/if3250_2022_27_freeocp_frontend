import styles from '../../../styles/Buttons.module.css';

const AddIconButton = ({ text, className, onClick, onMouseOver, style }) => {
	return (
		<button 
      className={[
        'flex items-center font-semibold', 
        className,
        styles.iconButtonText
      ].join(' ')}
      onClick={onClick}
      onMouseOver={onMouseOver}
      style={style}
    >
      <img src={`accordions/icon_add.svg`} className={[styles.icon, 'mr-1 my-1'].join(' ')} />
      {text}
		</button>
	)
}

export default AddIconButton;