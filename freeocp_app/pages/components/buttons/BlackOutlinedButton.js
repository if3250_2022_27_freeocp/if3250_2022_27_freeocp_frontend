import styles from '../../../styles/Buttons.module.css';

const BlackOutlinedButton = ({ text, className, onClick, onMouseOver, style, blueHover, selected=false }) => {
	return (
		<button 
      className={[
        selected ? styles.primaryButton : blueHover ? styles.blackOutlinedHoverButton : styles.blackOutlinedButton,
        'rounded-3xl px-8 py-2.5', 
        className
      ].join(' ')}
      onClick={onClick}
      onMouseOver={onMouseOver}
      style={style}
    >
				{text}
		</button>
	)
}

export default BlackOutlinedButton;