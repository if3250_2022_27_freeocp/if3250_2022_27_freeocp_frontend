import styles from '../../../styles/Buttons.module.css';

const SecondaryDangerButton = ({ text, className, onClick, onMouseHover, disabled=false }) => {
	return (
		<button 
			className={[styles.secondaryDangerButton, 'rounded-3xl px-8 py-2.5', className].join(' ')}
			onClick={onClick}
      onMouseHover={onMouseHover}
			disabled={disabled}
		>
				{text}
		</button>
	)
}

export default SecondaryDangerButton;