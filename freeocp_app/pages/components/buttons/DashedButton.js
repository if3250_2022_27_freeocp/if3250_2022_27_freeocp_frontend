import styles from '../../../styles/Buttons.module.css';

const DashedButton = ({ text, className, onClick, onMouseOver, style }) => {
	return (
		<button 
      className={[
        'rounded-3xl px-8 py-2.5', 
        className,
        styles.dashedButton
      ].join(' ')}
      onClick={onClick}
      onMouseOver={onMouseOver}
      style={style}
    >
				{text}
		</button>
	)
}

export default DashedButton;