import styles from '../../../styles/Buttons.module.css';

const PrimaryButton = ({ text, className, onClick, onMouseHover, disabled=false }) => {
	return (
		<button 
			className={[styles.primaryButton, className, 'rounded-3xl px-8 py-2.5'].join(' ')}
			onClick={onClick}
      onMouseHover={onMouseHover}
			disabled={disabled}
		>
				{text}
		</button>
	)
}

export default PrimaryButton;