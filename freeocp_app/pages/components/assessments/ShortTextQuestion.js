import styles from '../../../styles/Buttons.module.css';
import SectionContainer from '../containers/SectionContainer';
import { useSelector, useDispatch } from "react-redux";
import { deleteQuestion, changeQuestionDescription, changeShortTextSolution } from '../../../redux/actions/CreateAssessmentAction';

const ShortTextQuestion = ({ questionIdx }) => {
	if (questionIdx == null) {
    return null
  }
	const dispatch = useDispatch()
	const description = useSelector(state => state.questions[questionIdx].description)
	const solution = useSelector(state => state.questions[questionIdx].solution)
	return (
		<div className='my-6 flex items-start'>
			<SectionContainer title={`${questionIdx + 1} (Isian)`}>
				<div className='py-6 px-20'>
					<div className='text-2xl font-medium mb-1'>Deskripsi</div>
					<div className='flex flex-row rounded-3xl w-full grow px-3 py-1 border-gray-800' style={{ borderWidth: '1px' }}>
						<textarea 
							className={['focus:outline-none text-xl w-full text-gray-700 mx-2', styles.growArea].join(' ')} 
							style={{ resize: "none" }}
							value={description}
							onChange={(e) => dispatch(changeQuestionDescription(questionIdx, e.target.value))}
							rows={6}
							id='growArea'
						></textarea>
					</div>
					<div className='text-2xl font-medium mb-1 mt-4'>Jawaban</div>
          <div className='flex flex-row rounded-3xl w-full grow px-3 py-1 border-gray-800' style={{ borderWidth: '1px' }}>
            <input 
              className="appearance-none py-1 text-xl w-full mx-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
              type="text" 
              placeholder="Kunci Jawaban"
              value={solution}
              onChange={(e) => dispatch(changeShortTextSolution(questionIdx, e.target.value))}
            ></input>
          </div>
				</div>
			</SectionContainer>
			<img
				src='assessments/icon_remove.svg'
				className='cursor-pointer ml-2'
				onClick={() => dispatch(deleteQuestion(questionIdx))}
			/>
		</div>
	)
}

export default ShortTextQuestion;