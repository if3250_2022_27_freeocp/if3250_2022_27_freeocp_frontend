import React from 'react';
import styles from '../../../styles/Assessments.module.css';
import { useSelector, useDispatch } from "react-redux";
import { changeMultipleChoiceSolution, changeQuestionOption, deleteQuestionOption } from '../../../redux/actions/CreateAssessmentAction';

const RadioButtonOption = ({ optionIdx, name }) => {
  if (optionIdx == null || name == null) {
    return null
  }
  const dispatch = useDispatch()
  const options = useSelector(state => state.questions[name].options)
  const solution = useSelector(state => state.questions[name].solution)
	return (
    <div className='flex my-3'>
      <div 
        className={['flex flex-row rounded-3xl w-full grow border-gray-800 items-center',
        (options[optionIdx].text === '' || !options[optionIdx].text) && 'h-12', 
        options.length > 1 && 'mr-2'].join(' ')}
        style={{ borderWidth: '1px' }}
      >
        <input 
          type="radio" 
          checked={solution.index === optionIdx}
          onChange={() => dispatch(changeMultipleChoiceSolution(name, optionIdx, options[optionIdx]))}
          name={name}
          className='ml-3 mr-2 cursor-pointer'
        ></input>
        <div className={[styles.optionAlphabet, 'w-10 mr-2 flex items-center justify-center'].join(' ')}>
          {String.fromCharCode(optionIdx + 65)}.
        </div>
        <input 
          className="appearance-none py-2 text-lg w-full mr-6 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
          type="text" 
          placeholder="Pilihan Jawaban"
          value={options[optionIdx]}
          onChange={(e) => dispatch(changeQuestionOption(name, optionIdx, e.target.value, 'multiple-choice'))}
        ></input>
      </div>
      {options.length > 1 &&
        <img
          src='assessments/icon_remove.svg'
          className='cursor-pointer'
          onClick={() => dispatch(deleteQuestionOption(name, optionIdx))}
        />
      }
    </div>
	)
}

export default RadioButtonOption;
