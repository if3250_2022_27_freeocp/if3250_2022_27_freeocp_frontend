import React from 'react';
import styles from '../../../styles/Buttons.module.css';
import SectionContainer from '../containers/SectionContainer';
import RadioButtonOption from './RadioButtonOption';
import DashedButton from '../buttons/DashedButton';
import { useSelector, useDispatch } from "react-redux";
import { addQuestionOption, deleteQuestion, changeQuestionDescription } from '../../../redux/actions/CreateAssessmentAction';

const MultipleChoiceQuestion = ({ questionIdx }) => {
	if (questionIdx == null) {
    return null
  }
	const dispatch = useDispatch()
	const description = useSelector(state => state.questions[questionIdx].description)
	const options = useSelector(state => state.questions[questionIdx].options)
	const addOptionHandler = () => {
		dispatch(addQuestionOption(questionIdx))
	}
	return (
		<div className='my-6 flex items-start'>
			<SectionContainer title={`${questionIdx + 1} (Pilihan Ganda)`}>
				<div className='py-6 px-20'>
					<div className='text-2xl font-medium mb-1'>Deskripsi</div>
					<div className='flex flex-row rounded-3xl w-full grow px-3 py-1 border-gray-800' style={{ borderWidth: '1px' }}>
						<textarea 
							className={['focus:outline-none text-xl w-full text-gray-700 mx-2', styles.growArea].join(' ')} 
							style={{ resize: "none" }}
							value={description}
							onChange={(e) => dispatch(changeQuestionDescription(questionIdx, e.target.value))}
							rows={6}
							id='growArea'
						></textarea>
					</div>
					<div className='text-2xl font-medium mb-1 mt-4'>Pilihan Jawaban</div>
					<ul className='list-disc blue-text ml-4 font-medium mb-2'>
						<li>Klik ikon lingkaran untuk menandai pilihan yang benar</li>
					</ul>
					{options.map((value, idx) => (
						<RadioButtonOption 
							optionIdx={idx}
							name={questionIdx}
						/>
					))}
					<DashedButton text='Tambah' className='w-full' onClick={addOptionHandler} />
				</div>
			</SectionContainer>
			<img
				src='assessments/icon_remove.svg'
				className='cursor-pointer ml-2'
				onClick={() => dispatch(deleteQuestion(questionIdx))}
			/>
		</div>
	)
}

export default MultipleChoiceQuestion;