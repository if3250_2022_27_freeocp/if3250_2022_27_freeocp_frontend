import React from 'react';
import styles from '../../../styles/Assessments.module.css';
import { useSelector, useDispatch } from "react-redux";
import { addMultipleAnswerSolution, deleteMultipleAnswerSolution, changeQuestionOption, deleteQuestionOption } from '../../../redux/actions/CreateAssessmentAction';
const CheckboxButtonOption = ({ optionIdx=null, name=null }) => {
  if (optionIdx == null || name == null) {
    return null
  }
	const dispatch = useDispatch()
  const options = useSelector(state => state.questions[name].options)
  const solutions = useSelector(state => state.questions[name].solutions)
  const isOptionChecked = () => {
    let found = false
    let solutionIndex = -1
    for (let i=0;i<solutions.length;i++) {
      if (solutions[i].index === optionIdx) {
        found = true
        solutionIndex = i
        break
      }
    }
    return {
      found,
      solutionIndex
    }
  }
  const handleCheckOption = () => {
    let { found, solutionIndex } = isOptionChecked()
    if (found) {
      dispatch(deleteMultipleAnswerSolution(name, solutionIndex))
    } else {
      dispatch(addMultipleAnswerSolution(name, optionIdx, options[optionIdx]))
    }
  }
  return (
    <div className='flex my-3'>
      <div 
        className={['flex flex-row rounded-3xl w-full grow border-gray-800 items-center',
        (options[optionIdx].text === '' || !options[optionIdx].text) && 'h-12', 
        options.length > 1 && 'mr-2'].join(' ')}
        style={{ borderWidth: '1px' }}
      >
        <input 
          type="checkbox" 
          checked={solutions.find((solution) => {
            return solution.index === optionIdx
          })}
          onChange={() => handleCheckOption()}
          name={name}
          className='ml-3 mr-2 cursor-pointer form-check-input'
        ></input>
        <div className={[styles.optionAlphabet, 'w-10 mr-2 flex items-center justify-center'].join(' ')}>
          {String.fromCharCode(optionIdx + 65)}.
        </div>
        <input 
          className="appearance-none py-2 text-lg w-full mr-6 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
          type="text" 
          placeholder="Pilihan Jawaban"
          value={options[optionIdx]}
          onChange={(e) => dispatch(changeQuestionOption(name, optionIdx, e.target.value, 'multiple-answer'))}
        ></input>
      </div>
      {options.length > 1 &&
        <img
          src='assessments/icon_remove.svg'
          className='cursor-pointer'
          onClick={() => {
            dispatch(deleteQuestionOption(name, optionIdx))
            handleCheckOption()
          }}
        />
      }
    </div>
	)
}

export default CheckboxButtonOption;
