import styles from '../../../styles/Card.module.css';

const HistoryCard = ({ className, onClick, historyData }) => {
  const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  if (historyData != null) {
    return (
      <div
        className={[styles.historyCardContainer, 'rounded-3xl', className].join(' ')}
        onClick={onClick}
      >
        <img src={historyData.thumbnail} className={[styles.thumbnail, 'rounded-t-3xl'].join(' ')} />
        <div className='px-4 py-2'>
          <p className='text-lg line-clamp-1'>{historyData.title}</p>
          <p className='blue-text text-sm font-bold line-clamp-1'>{historyData.topic}</p>
          <p className='gray-text text-xs line-clamp-2 italic h-10'>Pembuat: {historyData.creator}</p>

          <div className={['w-full bg-gray-200 rounded-full dark:bg-gray-700', styles.progressBarContainer].join(' ')}>
            <div className={['bg-blue-600 text-sm font-medium text-center p-2 leading-none rounded-full', styles.progressBarChild].join(' ')} style={{width: `${historyData.progress}%`}}> {historyData.progress}%</div>
          </div>

          <p className='line-clamp-1 mt-4 text-sm'>Materi: {historyData.material_done} / {historyData.total_material}</p>
          <p className='line-clamp-1 text-sm'>Latihan: {historyData.assessment_done} / {historyData.total_assessment}</p>
          <p className='line-clamp-1 mb-4 text-sm'>Bab: {historyData.chapter_done} / {historyData.total_chapter}</p>

          <p className='line-clamp-1 text-sm'>Mulai: {new Date(historyData.start_date).toLocaleDateString("id-ID", dateOptions)}</p>
          <p className='line-clamp-1 text-sm mb-2'>Selesai: {historyData.finish_date ? new Date(historyData.finish_date).toLocaleDateString("id-ID", dateOptions) : '-'}</p>
        </div>
      </div>
    )
  }
  return null
}

export default HistoryCard;