import Router from 'next/router'
import styles from '../../../styles/Card.module.css';

const CourseCard = ({ className, onClick, courseData, blueHover=true }) => {
  const handleCourse = (courseId) => {
    Router.push({
      pathname: '/course',
      query: { courseId: courseId },
    })
  }
  const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  if (courseData != null) {
    return (
      <div
        className={[blueHover ? styles.courseCardContainer : styles.courseCardContainerWhiteHover, 'rounded-3xl cursor-pointer', className].join(' ')}
        onClick={() => handleCourse(courseData._id)}
      >
        <img src={courseData.thumbnail} className={[styles.thumbnail, 'rounded-t-3xl'].join(' ')} />
        <div className='px-4 py-2'>
          <p className='text-lg line-clamp-1'>{courseData.title}</p>
          <p className='blue-text text-sm font-bold line-clamp-1'>{courseData.topic}</p>
          <p className='gray-text text-sm line-clamp-2 italic h-10'>Pembuat: {courseData.creator}</p>
          <p className='gray-text text-sm my-2 line-clamp-4 h-20'>{courseData.description}</p>
          <p className='line-clamp-1'>Dibuat: {new Date(courseData.created_at).toLocaleDateString("id-ID", dateOptions)}</p>
          <p className='line-clamp-1'>Perubahan: {courseData.last_updated_at ? new Date(courseData.last_updated_at).toLocaleDateString("id-ID", dateOptions) : '-'}</p>
          <p className='line-clamp-1 mb-3'>Dipelajari: {courseData.taken_count} orang</p>
        </div>
      </div>
    )
  }
  return null
}

export default CourseCard;
