import styles from '../../../styles/Card.module.css';
import BlackOutlinedButton from '../buttons/BlackOutlinedButton';

const ContributionCard = ({ className, onClick, contributionData }) => {
  const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  if (contributionData != null) {
    return (
      <div
        className={[styles.courseCardContainer, 'rounded-3xl', className].join(' ')}
        onClick={onClick}
      >
        <img src={contributionData.thumbnail} className={[styles.thumbnail, 'rounded-t-3xl'].join(' ')} />
        <div className='px-4 py-2'>
          <p className='text-lg line-clamp-1'>{contributionData.title}</p>
          <p className='blue-text text-sm font-bold line-clamp-1'>{contributionData.topic}</p>
          <p className='gray-text text-sm line-clamp-2 italic h-10'>Pembuat: {contributionData.creator}</p>
          <p className='gray-text text-sm my-2 line-clamp-4 h-20'>{contributionData.description}</p>
          <p className='line-clamp-1 text-sm'>Dibuat: {new Date(contributionData.created_at).toLocaleDateString("id-ID", dateOptions)}</p>
          <p className='line-clamp-1 text-sm'>Perubahan: {contributionData.last_updated_at ? new Date(contributionData.last_updated_at).toLocaleDateString("id-ID", dateOptions) : '-'}</p>
          <p className='line-clamp-1 text-sm mb-4'>Dipelajari: {contributionData.taken_count} orang</p>
          <div className='flex mb-2'>
            <BlackOutlinedButton text='Ubah' className='text-sm mr-2 px-0 w-24' style={{ background: '#51C4D3', paddingLeft: 0, paddingRight: 0  }} />
            <BlackOutlinedButton text='Hapus' className='text-sm py-2 px-0 w-24' style={{ background: 'rgba(255, 0, 0, 0.6)', paddingLeft: 0, paddingRight: 0 }} />
          </div>
        </div>
      </div>
    )
  }
  return null
}

export default ContributionCard;