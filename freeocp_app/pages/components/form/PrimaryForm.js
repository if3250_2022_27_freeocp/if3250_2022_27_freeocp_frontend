const PrimaryForm = ({ text, desc, id, type, onChange, value }) => {
	return (
		<form>
			<label>
				{text}
			</label>
			<div className='text-xs text-gray-500'>
				{desc}
			</div>
			<div className='flex flex-row rounded-2xl px-1 py-1 mt-1 mb-3 border-gray-800' style={{ width: "450px", borderWidth: '1px' }}>
			<input className="appearance-none w-full ml-2 mt-1 mb-1 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id={id} type={type} onChange={onChange} value={value}></input>
			</div>
		</form>
	)
}

export default PrimaryForm;
