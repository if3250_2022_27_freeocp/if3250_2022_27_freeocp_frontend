import { useState } from "react";
import styles from '../../../styles/Register.module.css';

const PasswordForm = ({ text, desc, id, onChange, value }) => {
	const [passwordShown, setPasswordShown] = useState(false);
	const togglePassword = (e) => {
		e.preventDefault();
		setPasswordShown(!passwordShown);
	};
	return (
		<form>
			<label>
				{text}
			</label>
			<div className='text-xs text-gray-500'>
				{desc}
			</div>
			<div className='flex flex-row rounded-2xl px-1 py-1 mt-1 mb-3 border-gray-800' style={{ width: "450px", borderWidth: '1px' }}>
				<input
					className="appearance-none w-full ml-2 mt-1 mb-1 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
					id={id}
					type={passwordShown ? "text" : "password"}
					onChange={onChange} value={value}
					>
				</input>
				<img
					className={['cursor-pointer mr-1', styles.image].join(' ')}
			        src={passwordShown ? "register/show.png" : "register/hide.png"}
					onClick={togglePassword}>
				</img>
			</div>
		</form>
	)
}

export default PasswordForm;
