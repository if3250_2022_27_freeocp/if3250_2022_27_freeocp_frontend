import React from "react";
import Head from "next/head";
import { useRouter } from 'next/router'
import Navbar from "./components/navigations/Navbar";
import SecondaryButton from "./components/buttons/SecondaryButton";
import PrimaryButton from './components/buttons/PrimaryButton'
import styles from "../styles/Course.module.css";
import AccordionList from './components/accordions/AccordionList'
import AccordionListEdit from "./components/accordions/AccordionListEdit";

// Change values
import { createStore } from "redux";
import { CreateCourseReducer } from "../redux/reducers/CreateCourseReducer";
import { Provider, useSelector, useDispatch } from "react-redux";
import { changeTitle, changeTopic, changeDescription, changeSyllabus, changeCourseId, changeAllowAssessment } from "../redux/actions/CreateCourseAction";
import { updateCourse, getCourse, enrollCourse, createChapter, createMaterial, deleteChapter } from "./api/course";
import { getProfile } from "./api/profile";
import Footer from "./components/navigations/Footer";

export default function Course() {
  const store = createStore(CreateCourseReducer)
  return (
    <Provider store={store}>
      <CourseChild />
    </Provider>
  )
}
export function CourseChild() {
  const router = useRouter()
  const dispatch = useDispatch()
  // course data
  const [courseId, setCourseId] = React.useState(false);
  const [courseThumbnail, setCourseThumbnail] = React.useState(false);
  const [initCourseTitle, setInitCourseTitle] = React.useState(false);
  const courseTitle = useSelector(state => state.title)
  const [initCourseTopic, setInitCourseTopic] = React.useState(false);
  const courseTopic = useSelector(state => state.topic)
  const [initCourseDescription, setInitCourseDescription] = React.useState(false);
  const courseDescription = useSelector(state => state.description)
  const [courseContributor, setCourseContributor] = React.useState(false);
  const [isContributor, setIsContributor] = React.useState(false);
  const [courseSyllabus, setCourseSyllabus] = React.useState([]);
  const courseSyllabusText = useSelector(state => state.syllabus)
  const [initCourseMaterial, setInitCourseMaterial] = React.useState([]);
  const [courseMaterial, setCourseMaterial] = React.useState([]);
  const [courseBanner, setCourseBanner] = React.useState(false);

  // modals
  const [changeNameModalVisible, setChangeNameModalVisible] = React.useState(false)

  // inline edit
  const [learn, setLearn] = React.useState(false);
  const [showProgress, setShowProgress] = React.useState(false);
  const [edit, setEdit] = React.useState(false);
  const [editTitle, setEditTitle] = React.useState(false);
  const [editTopic, setEditTopic] = React.useState(false);
  const [editDescription, setEditDescription] = React.useState(false);
  const [editSyllabus, setEditSyllabus] = React.useState(false);
  const [editMaterial, setEditMaterial] = React.useState(false);
  const [errorTitle, setErrorTitle] = React.useState(false);
  const [errorTopic, setErrorTopic] = React.useState(false);
  const [errorDescription, setErrorDescription] = React.useState(false);
  const [errorSyllabus, setErrorSyllabus] = React.useState(false);

  const toggleShowProgress = (e) => {
    e.preventDefault();
    setShowProgress(!showProgress);
  };
  const toggleEdit = (e) => {
    e.preventDefault();
    setEdit(!edit);
  };

  const toggleTitle = (e) => {
    e.preventDefault();
    setEditTitle(!editTitle);
    setErrorTitle(false);
    setInitCourseTitle(courseTitle);
  };
  const initTitle = (e) => {
    e.preventDefault();
    setEditTitle(!editTitle);
    setErrorTitle(false);
    dispatch(changeTitle(initCourseTitle));
  };
  const emptyTitle = (e) => {
    e.preventDefault();
    setErrorTitle(true);
  };

  const toggleTopic = (e) => {
    e.preventDefault();
    setEditTopic(!editTopic);
    setErrorTopic(false);
    setInitCourseTopic(courseTopic);
  };
  const initTopic = (e) => {
    e.preventDefault();
    setEditTopic(!editTopic);
    setErrorTopic(false);
    dispatch(changeTopic(initCourseTopic));
  };
  const emptyTopic = (e) => {
    e.preventDefault();
    setErrorTopic(true);
  };

  const toggleDescription = (e) => {
    e.preventDefault();
    setEditDescription(!editDescription);
    setErrorDescription(false);
    setInitCourseDescription(courseDescription);
  };
  const initDescription = (e) => {
    e.preventDefault();
    setEditDescription(!editDescription);
    setErrorDescription(false);
    dispatch(changeDescription(initCourseDescription));
  };
  const emptyDescription = (e) => {
    e.preventDefault();
    setErrorDescription(true);
  };

  const toggleSyllabus = (e) => {
    e.preventDefault();
    let str = courseSyllabusText;
    var syllabi = str.split('- ');
    syllabi.shift();
    var x;
    for(x = 0; x < syllabi.length; x++)
    {
      syllabi[x] = syllabi[x].replace('\n', '');
    }
    {syllabi.length > 0 ?
      (
        setCourseSyllabus(syllabi),
        setErrorSyllabus(false),
        setEditSyllabus(!editSyllabus)
      )
    :
      dispatch(changeSyllabus(""));
      setErrorSyllabus(true);
    }
  };
  const initSyllabus = (e) => {
    e.preventDefault();
    setErrorSyllabus(false);
    setEditSyllabus(!editSyllabus);
    var courseData = courseSyllabus;
    var txt = "";
    courseData.forEach(d => txt += "- " + d + "\n");
    dispatch(changeSyllabus(txt));
  };

  const toggleMaterial = (e) => {
    e.preventDefault();
    setEditMaterial(!editMaterial);
  };
  const initMaterial = (e) => {
    e.preventDefault();
    getCourse(courseId).then(function(res) {
      const { course, data } = res.data
      setCourseMaterial(data)
    }).catch(function(err) {
      console.log(err)
    })
    setEditMaterial(!editMaterial);
  };

  const handleEnroll = () => {
    console.log(courseId)
    // e.preventDefault();
    enrollCourse(courseId).then(function(res) {
      console.log(res)
    }).catch(function(err) {
      console.log(err)
    })
    // setLearn(!learn);
  }

  const handleUpdateCourse = async() => {
    setEdit(!edit);
    for (let chapter of courseMaterial) {
      await deleteChapter(courseId, chapter.chapter_id).then(function(res) {
        console.log(res)
      }).catch(function(err) {
        console.log(err)
        return
      })
    }
    let chapters_id = []
    let materials = []
    for (let chapter of courseMaterial) {
      let materials_id = []
      for (let material of chapter.content) {
        materials.push(material)
        await createMaterial(material.title, material.type, material.link).then(function(res) {
          console.log(res)
          materials_id.push(res.data.material._id)
        }).catch(function(err) {
          console.log(err)
          return
        })
      }
      await createChapter(chapter.chapter_title, materials_id).then(function(res) {
        console.log(res)
        chapters_id.push(res.data.chapter._id)
      }).catch(function(err) {
        console.log(err)
        return
      })
    }
    await updateCourse(courseId,courseTitle,courseTopic,courseThumbnail,courseDescription,courseSyllabus,chapters_id,materials).then(function(res) {
      console.log(res)
      // setShowModal(true)
    }).catch(function(err) {
      console.log(err)
      return
    })
  }

  React.useEffect(() => {
    dispatch(changeAllowAssessment(true))
    setCourseId(router.query.courseId)
    if (router.query.courseId) {
      dispatch(changeCourseId(router.query.courseId))
    }
    getCourse(router.query.courseId).then(function(res) {
      console.log(res.data)
      const { course, data, is_contributor, is_enrolled } = res.data
      setIsContributor(is_contributor)
      setLearn(is_enrolled)
      setInitCourseTitle(course.title)
      dispatch(changeTitle(course.title))
      setInitCourseTopic(course.topic)
      dispatch(changeTopic(course.topic))
      setInitCourseDescription(course.description)
      dispatch(changeDescription(course.description))
      setCourseSyllabus(course.syllabus)
      var courseData = course.syllabus
      var txt = ""
      courseData.forEach(d => txt += "- " + d + "\n")
      dispatch(changeSyllabus(txt))
      setInitCourseMaterial(data)
      setCourseMaterial(data)
      setCourseContributor(course.contributor_name)
      setCourseThumbnail(course.thumbnail)
      setCourseBanner("url("+course.thumbnail+")")
    }).catch(function(err) {
      console.log(err)
    })
  }, [router])
  return (
    <div>
      <Head>
        <title>{courseTitle}</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500&display=swap" rel="stylesheet" />
      </Head>

      <div className='navbar-margin'>
        <Navbar spaceBottom />
      </div>

      <div className='w-full'>
        <div style={{
          backgroundImage: courseBanner,
          height: "150px",
          backgroundAttachment: "fixed",
          backgroundPosition: "center top",
          backgroundRepeat: "no-repeat",
          backgroundColor: "rgba(81, 196, 211, 0.25)"
        }}>
        </div>
        <div>
          <div className={['px-56 flex'].join(' ')}>
            <div className={['flex flex-col mt-2 grow'].join(' ')}>
              {edit ?
                <span
                  className='gray-text underline mb-1 text-sm cursor-pointer whitespace-nowrap'
                  onClick={() => setChangeNameModalVisible(true)}
                >
                  Ubah Banner
                </span>
              :
                <span
                  className='mb-1'
                  style={{ height: "21px" }}
                >
                </span>
              }
              <div className={['flex justify-between items-start'].join(' ')}>
                <div className='flex flex-col grow'>
                  {editTitle && edit ?
                    <div className={['', styles.parent0].join(' ')}>
                      <div className='flex flex-row rounded-2xl border-gray-800' style={{ width: "100%", borderWidth: '1px' }}>
                        {errorTitle ?
                          <input
                            className={['appearance-none w-full ml-2 mt-1 mb-1 leading-tight focus:outline-none focus:shadow-outline text-3xl', styles.input].join(' ')}
                            type='input'
                            placeholder="Judul Kursus tidak boleh kosong"
                            onChange={(e) => dispatch(changeTitle(e.target.value))}
                          />
                        :
                          <input
                            className={['appearance-none w-full ml-2 mt-1 mb-1 leading-tight focus:outline-none focus:shadow-outline text-3xl', styles.input].join(' ')}
                            type='input'
                            value={ courseTitle }
                            onChange={(e) => dispatch(changeTitle(e.target.value))}
                          />
                        }
                      </div>
                      <div className={['', styles.child0].join(' ')}>
                        <div className={['cursor-pointer mr-2 mt-2', styles.parent1].join(' ')}>
                          <img
                            className={['', styles.imageButton].join(' ')}
                            src="edit/cross.png">
                          </img>
                          <img
                            className={['', styles.learnButton].join(' ')}
                            src="edit/crossHover.png"
                            onClick={initTitle}>
                          </img>
                        </div>
                        <div className={['cursor-pointer mt-2', styles.parent1].join(' ')}>
                          <img
                            className={['', styles.imageButton].join(' ')}
                            src="edit/check.png">
                          </img>
                          <img
                            className={['', styles.learnButton].join(' ')}
                            src="edit/checkHover.png"
                            onClick={courseTitle ? toggleTitle : emptyTitle}>
                          </img>
                        </div>
                      </div>
                    </div>
                  : <p>
                      <span className='text-3xl mr-3'>{courseTitle}</span>
                      {edit &&
                        <span
                          className='gray-text underline mt-2 text-sm cursor-pointer whitespace-nowrap'
                          onClick={toggleTitle}
                        >
                          Ubah Judul
                        </span>
                      }
                    </p>}
                  {editTopic && edit ?
                    <div className={['', styles.parent0].join(' ')}>
                      <div className='flex flex-row rounded-2xl border-gray-800' style={{ width: "100%", borderWidth: '1px' }}>
                        {errorTopic ?
                          <input className={['appearance-none w-full ml-2 mt-1 mb-1 leading-tight focus:outline-none focus:shadow-outline font-medium text-xl blue-text', styles.input].join(' ')}
                            type='text'
                            placeholder="Topik Kursus tidak boleh kosong"
                            onChange={(e) => dispatch(changeTopic(e.target.value))}
                          />
                        :
                          <input className={['appearance-none w-full ml-2 mt-1 mb-1 leading-tight focus:outline-none focus:shadow-outline font-medium text-xl blue-text', styles.input].join(' ')}
                            type='text'
                            value={ courseTopic }
                            onChange={(e) => dispatch(changeTopic(e.target.value))}
                          />
                        }
                      </div>
                      <div className={['', styles.child0].join(' ')}>
                        <div className={['cursor-pointer mr-2 mt-2', styles.parent1].join(' ')}>
                          <img
                            className={['', styles.imageButton].join(' ')}
                            src="edit/cross.png">
                          </img>
                          <img
                            className={['', styles.learnButton].join(' ')}
                            src="edit/crossHover.png"
                            onClick={initTopic}>
                          </img>
                        </div>
                        <div className={['cursor-pointer mt-2', styles.parent1].join(' ')}>
                          <img
                            className={['', styles.imageButton].join(' ')}
                            src="edit/check.png">
                          </img>
                          <img
                            className={['', styles.learnButton].join(' ')}
                            src="edit/checkHover.png"
                            onClick={courseTopic ? toggleTopic : emptyTopic}>
                          </img>
                        </div>
                      </div>
                    </div>
                  : <p>
                      <span className='font-medium text-xl mr-3 blue-text'>{courseTopic}</span>
                      {edit &&
                        <span
                          className='gray-text underline mt-2 text-sm cursor-pointer whitespace-nowrap'
                          onClick={toggleTopic}
                        >
                          Ubah Topik
                        </span>
                      }
                    </p>
                  }
                </div>
                <div className='flex'>
                  {edit ?
                    <SecondaryButton
                      text='Kembali'
                      className='ml-8'
                      onClick={handleUpdateCourse}
                    />
                  :
                    <>
                      {isContributor ?
                        <SecondaryButton
                          text='Ubah'
                          className='ml-8'
                          onClick={toggleEdit}
                        />
                        :
                          <>
                            {learn ?
                              <div className={['ml-2', styles.parent1].join(' ')}>
                                {showProgress ?
                                  <img
                                    className={['', styles.imageButton].join(' ')}
                                    src="edit/learn.png">
                                  </img>
                                :
                                  <>
                                    <img
                                      className={['cursor-pointer', styles.imageButton].join(' ')}
                                      src="edit/learn.png"
                                      onClick={toggleShowProgress}>
                                    </img>
                                    <img
                                      className={['cursor-pointer', styles.learnButton].join(' ')}
                                      src="edit/learnHover.png"
                                      onClick={toggleShowProgress}>
                                    </img>
                                  </>
                                }
                                <div className={['', styles.child1].join(' ')}>
                                  {showProgress &&
                                    <>
                                      <div className='absolute left-0'>
                                        <div
                                          className="px-2.5 py-2.5 rounded-t-2xl flex justify-between items-start"
                                          style={{ background: '#126E82'}}
                                        >
                                          <p className='text-sm flex flex-col grow' style={{ width: '180px', color: 'white'}}>
                                            Progres Belajar
                                          </p>
                                          <img
                                            className={['cursor-pointer flex flex-col grow', styles.cancelButton].join(' ')}
                                            src='edit/cancel_icon.png'
                                            onClick={toggleShowProgress}
                                          />
                                        </div>
                                        <div
                                          className="px-1.5 py-1.5 rounded-b-2xl"
                                          style={{ background: 'white', border: '1px solid #126E82'}}
                                        >
                                          <div className="relative" style={{ height:'29px' }}>
                                            <div
                                              className="absolute left-0 rounded-xl text-sm mb-1.5 flex flex-col items-center grow"
                                              style={{ background: '#126E82', height:'23px', width: '188px', color: 'white', border: '1px solid black'}}
                                            >
                                              <p>0%</p>
                                            </div>
                                            {false &&
                                              <div
                                                className="rounded-xl text-sm flex flex-col items-center absolute left-0"
                                                style={{ background: '#51C4D3', height:'23px', width:'50px', color: 'white', border: '1px solid black'}}
                                              >
                                              </div>
                                            }
                                          </div>
                                          <div
                                            className="flex justify-between items-start"
                                          >
                                            <div
                                              className="rounded-xl text-xs items-center flex flex-col grow"
                                              style={{ background: 'white', width: "60px", border: '1px solid black'}}
                                            >
                                              <img
                                                className={['mt-0.5 mb-0.5', styles.icon].join(' ')}
                                                src='accordions/icon_article.png'
                                              />
                                              <p>
                                                0 / 3
                                              </p>
                                            </div>
                                            <div
                                              className="rounded-xl mr-1 ml-1 text-xs items-center flex flex-col grow"
                                              style={{ background: 'white', width: "60px", border: '1px solid black'}}
                                            >
                                              <img
                                                className={['mt-0.5 mb-0.5', styles.icon].join(' ')}
                                                src='accordions/icon_video.png'
                                              />
                                              <p>
                                                0 / 3
                                              </p>
                                            </div>
                                            <div
                                              className="rounded-xl text-xs items-center flex flex-col grow"
                                              style={{ background: 'white', width: "60px", border: '1px solid black'}}
                                            >
                                              <img
                                                className={['mt-0.5 mb-0.5', styles.icon].join(' ')}
                                                src='accordions/icon_assessment.png'
                                              />
                                              <p>
                                                0 / 1
                                              </p>
                                            </div>
                                          </div>
                                          <div
                                            className="px-2 py-0.5 rounded-xl text-xs mt-1.5 flex flex-col grow"
                                            style={{ background: '#D7F1F5', border: '1px solid black'}}
                                          >
                                            Mulai: 22 April 2022
                                          </div>
                                          <div
                                            className="px-2 py-0.5 rounded-xl text-xs mt-1.5 flex flex-col grow"
                                            style={{ background: 'white', border: '1px solid black'}}
                                          >
                                            Selesai: -
                                          </div>
                                        </div>
                                      </div>
                                    </>
                                  }
                                </div>
                              </div>
                            :
                              <PrimaryButton
                                text='Pelajari'
                                className='ml-2'
                                onClick={() => handleEnroll()}
                              />
                            }
                          </>
                      }
                    </>
                  }
                </div>
              </div>
              <div class="py-4">
                  <div class="w-full border-t mb-1 border-gray-700"></div>
              </div>
              <div>
                <img className={['mb-5 mr-3', styles.img].join(' ')} src='profile/avatar.png' />
                <p className='text-sm mt-3'>
                  Dibuat oleh:
                </p>
                <span className='text-xl'>{courseContributor}</span>
              </div>
              <p>
                <span className='text-2xl font-bold mr-3'>Deskripsi</span>
                {!editDescription && edit &&
                  <span
                    className='gray-text underline mt-2 text-sm cursor-pointer whitespace-nowrap'
                    onClick={toggleDescription}
                  >
                    Ubah Deskripsi
                  </span>
                }
              </p>
              {editDescription && edit ?
                <div className={['mt-2 mb-6', styles.parent0].join(' ')}>
                  <div className='rounded-2xl border-gray-800' style={{ width: "100%", height: "150px", borderWidth: '1px' }}>
                    {errorDescription ?
                      <textarea className={['appearance-none w-full ml-2 mt-1 mb-1 leading-tight focus:outline-none focus:shadow-outline mx-2', styles.input].join(' ')}
                        style={{ resize: "none" }}
                        type='text'
                        placeholder="Deskripsi Kursus tidak boleh kosong"
                        onChange={(e) => dispatch(changeDescription(e.target.value))}
                        rows={6}
                      ></textarea>
                    :
                      <textarea className={['appearance-none w-full ml-2 mt-1 mb-1 leading-tight focus:outline-none focus:shadow-outline mx-2', styles.input].join(' ')}
                        style={{ resize: "none" }}
                        type='text'
                        value={ courseDescription }
                        onChange={(e) => dispatch(changeDescription(e.target.value))}
                        rows={6}
                      ></textarea>
                    }
                  </div>
                  <div className={['', styles.child0].join(' ')}>
                    <div className={['cursor-pointer mr-2 mt-2', styles.parent1].join(' ')}>
                      <img
                        className={['', styles.imageButton].join(' ')}
                        src="edit/cross.png">
                      </img>
                      <img
                        className={['', styles.learnButton].join(' ')}
                        src="edit/crossHover.png"
                        onClick={initDescription}>
                      </img>
                    </div>
                    <div className={['cursor-pointer mt-2', styles.parent1].join(' ')}>
                      <img
                        className={['', styles.imageButton].join(' ')}
                        src="edit/check.png">
                      </img>
                      <img
                        className={['', styles.learnButton].join(' ')}
                        src="edit/checkHover.png"
                        onClick={courseDescription ? toggleDescription : emptyDescription}>
                      </img>
                    </div>
                  </div>
                </div>
              : <div className={['mt-2 mb-8', styles.textarea].join(' ')}>{courseDescription}</div>
              }
              <p>
                <span className='text-2xl font-bold mr-3'>Silabus</span>
                {!editSyllabus && edit &&
                  <span
                    className='gray-text underline mt-2 text-sm cursor-pointer whitespace-nowrap'
                    onClick={toggleSyllabus}
                  >
                    Ubah Silabus
                  </span>
                }
              </p>
              {editSyllabus && edit ?
                <div className={['mb-6', styles.parent0].join(' ')}>
                  <p className='text-sm mt-1 mb-2 font-bold blue-text mr-3'>Gunakan tanda strip ‘-’ untuk menambahkan silabus</p>
                  <div className='rounded-2xl border-gray-800' style={{ width: "100%", height: "150px", borderWidth: '1px' }}>
                    {errorSyllabus ?
                      <textarea className={['appearance-none w-full ml-2 mt-1 mb-1 leading-tight focus:outline-none focus:shadow-outline mx-2', styles.inputSyllabus].join(' ')}
                        style={{ resize: "none" }}
                        type='text'
                        value={ courseSyllabusText }
                        placeholder="Silabus Kursus tidak boleh kosong"
                        onChange={(e) => dispatch(changeSyllabus(e.target.value))}
                        rows={6}
                      ></textarea>
                    :
                      <textarea className={['appearance-none w-full ml-2 mt-1 mb-1 leading-tight focus:outline-none focus:shadow-outline mx-2', styles.inputSyllabus].join(' ')}
                        style={{ resize: "none" }}
                        type='text'
                        value={ courseSyllabusText }
                        onChange={(e) => dispatch(changeSyllabus(e.target.value))}
                        rows={6}
                      ></textarea>
                    }
                  </div>
                  <div className={['', styles.child0].join(' ')}>
                    <div className={['cursor-pointer mr-2 mt-2', styles.parent1].join(' ')}>
                      <img
                        className={['', styles.imageButton].join(' ')}
                        src="edit/cross.png">
                      </img>
                      <img
                        className={['', styles.learnButton].join(' ')}
                        src="edit/crossHover.png"
                        onClick={initSyllabus}>
                      </img>
                    </div>
                    <div className={['cursor-pointer mt-2', styles.parent1].join(' ')}>
                      <img
                        className={['', styles.imageButton].join(' ')}
                        src="edit/check.png">
                      </img>
                      <img
                        className={['', styles.learnButton].join(' ')}
                        src="edit/checkHover.png"
                        onClick={toggleSyllabus}>
                      </img>
                    </div>
                  </div>
                </div>
              :
                <ul className={['mt-2 mb-8', styles.indent].join(' ')}>
                  {courseSyllabus.map((syllabus) => <li>{syllabus}</li>)}
                </ul>
              }
              <p>
                <span className='text-2xl font-bold mr-3'>Materi</span>
                {!editMaterial && edit &&
                  <span
                    className='gray-text underline text-sm cursor-pointer whitespace-nowrap'
                    onClick={toggleMaterial}
                  >
                    Ubah Materi
                  </span>
                }
              </p>
              {editMaterial && edit ?
                <div className={['mb-20', styles.parent0].join(' ')}>
                  <AccordionListEdit materialData={courseMaterial} />
                  <div className={['', styles.child0].join(' ')}>
                    <div className={['cursor-pointer mr-2 mt-2', styles.parent1].join(' ')}>
                      <img
                        className={['', styles.imageButton].join(' ')}
                        src="edit/cross.png">
                      </img>
                      <img
                        className={['', styles.learnButton].join(' ')}
                        src="edit/crossHover.png"
                        onClick={initMaterial}>
                      </img>
                    </div>
                    <div className={['cursor-pointer mt-2', styles.parent1].join(' ')}>
                      <img
                        className={['', styles.imageButton].join(' ')}
                        src="edit/check.png">
                      </img>
                      <img
                        className={['', styles.learnButton].join(' ')}
                        src="edit/checkHover.png"
                        onClick={toggleMaterial}>
                      </img>
                    </div>
                  </div>
                </div>
              :
                <div className='mb-8'>
                  <AccordionList materialData={courseMaterial} isEdit={edit} />
                </div>
              }
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
